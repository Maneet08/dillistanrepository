<?php include('../../config.php') ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>

<?php
  $Areas = getAreas();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Admin Area - Users </title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">

  <div class="col-md-8 col-md-offset-2">
  <?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
    <h1 class="text-center">Areas</h1>
    <hr>
    <br />
    <?php if (isset($areas)): ?>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>S. No</th>
            <th>Area Name</th>
            <th>Region Name</th>
            <th colspan="1" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($Areas as $key => $value): ?>
            <tr>
              <td><?php echo $key + 1; ?></td>
              <td><?php echo $value['area_name'] ?></td>
              <td><?php echo $value['region_name']; ?></td>
              <td class="text-center">
                <a href="areaForm.php?edit_area=<?php echo $value['id'] ?>" class="btn btn-sm btn-success">
                  <span class="glyphicon glyphicon-pencil"></span>
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h2 class="text-center">No Areas in database</h2>
    <?php endif; ?>
  </div>
  <?php include(INCLUDE_PATH . '/layouts/footer.php') ?>
</body>
</html>
