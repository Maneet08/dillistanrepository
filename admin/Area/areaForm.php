<?php include('../../config.php'); ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php include('areaLogic.php'); ?>
<?php $regions = getRegions(); ?>


<!DOCTYPE html>
<html>
  <head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <!-- Custome styles -->
    <link rel="stylesheet" href="../../assets/css/style.css">
  </head>
  <body style="margin: 5% 5% 5% 5%">
    <?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <?php if ($isEditing === true): ?>
            <h1 class="text-center" >Update Area</h1> 
        <?php else :?>
            <h1 class="text-center" >Add New Area</h1>
        <?php endif; ?>
            <hr><br>
            <form class="form" method="post" action="areaForm.php" >
                   <?php if(isset($allregions)) : ?>
            <?php if ($isEditing === true): ?>
              <input type="hidden" name="area_id" value="<?php echo $area_id ?>">
            <?php endif; ?>    
              <div class="form-group <?php echo isset($errors['area_name']) ? 'has-error' : '' ?>">
                    <label class="control-label"> Enter Area Name :</label>
                    <input type="text" name="area_name" class="form-control" value="<?php echo $area_name; ?>">
                    <?php if (isset($errors['area_name'])): ?>
                <span class="help-block"><?php echo $errors['area_name'] ?></span>
              <?php endif; ?>
             </div>
             <div class="form-group <?php echo isset($errors['region_id']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Select It's Region : </label> 
                    <select  name="region_id"  class="form-control">
                            <option value="">Select Region</option>
                            <?php foreach ($regions as $region): ?>
                            <option value="<?php echo $region['id']; ?>" <?php if($region['id'] == $region_id): echo "selected"; endif; ?>><?php echo $region['region_name']; ?></option>
                            <?php endforeach; ?>
                    </select>
                    <?php if (isset($errors['region_id'])): ?>
                <span class="help-block"><?php echo $errors['region_id'] ?></span>
              <?php endif; ?>
              </div>

             <div class="form-group">
             <?php if ($isEditing === true): ?>
                      <button type="submit" name="update_area" class="btn btn-success btn-block btn-lg">Update Area</button>
             <?php else : ?>
                    <button type="submit" name="save_area" class="btn btn-success btn-block btn-lg">Save Area</button>
             <?php endif; ?>       
             </div>
                   <?php endif;?>
            </form>
  
    </div>
  </body>
  </html>
  