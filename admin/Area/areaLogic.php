<?php 
$area_id = 0;
$area_name = "";
$region_id = "";
$errors= array();
$isEditing = false;


// ACTION: Save area
if(isset($_POST['save_area'])) { // if user clicked save_user button ... 
   saveArea();
  }

// ACTION: fetch area for editting
if(isset($_GET['edit_area'])) {
    $area_id = $_GET['edit_area'];
    editArea($area_id);
}

// ACTION: update area
if(isset($_POST['update_area'])) { // if user clicked update_user button ...
    $area_id = $_POST['area_id'];
    updateArea($area_id);
}

function saveArea(){
    global $errors, $area_name, $region_id;
    $errors = validateArea($_POST, ['save_area']);
    // receive all input values from the form
    $area_name = $_POST['area_name'];
    $region_id = $_POST['region_id'];
    if (count($errors) === 0) {
     // if (isset($_POST['role_id'])) {
       // $role_id = $_POST['role_id'];
     // }
      $sql = "INSERT INTO area SET area_name=?, region_id=?";
      $result = modifyRecord($sql, 'si', [$area_name, $region_id]);
  
      if($result){
        $_SESSION['success_msg'] = "New Area added successfully";
        header("location:areaList.php");
        exit(0);
      } else {
        $_SESSION['error_msg'] = "Something went wrong. Could not add area in Database";
      }
    }
  }

function editArea($area_id){
    global $area_id, $region_id, $area_name, $isEditing;
  
    $sql = "SELECT * FROM area WHERE id=?";
    $areas = getSingleRecord($sql, 'i', [$area_id]);
  
    $area_id = $areas['id'];
    $region_id = $areas['region_id'];
    $area_name = $areas['area_name'];
    $isEditing = true;
  }

  function updateArea($area_id) {
    global $errors, $area_name, $region_id, $isEditing;
    $errors = validateArea($_POST, ['update_area']);
  
    if (count($errors) === 0) {
    // receive all input values from the form
    $area_name = $_POST['area_name'];
    $region_id = $_POST['region_id']; 

      $sql = "UPDATE area SET area_name=?, region_id=? WHERE id=?";
      $result = modifyRecord($sql, 'sii', [$area_name, $region_id, $area_id]);
  
      if ($result) {
        $_SESSION['success_msg'] = "Area updated successfully";
        header("location: areaList.php");
        exit(0);
      }
    }
      else {
        // continue editting if there were errors
        $isEditing = true;
    } 
    }
  

?>