<?php

  // if user is NOT logged in, redirect them to login page
  if (!isset($_SESSION['user'])) {
    header("location: ".BASE_URL."login.php");
  }
  // if user is logged in and this user is NOT an admin user, redirect them to landing page
  if (isset($_SESSION['user']) && is_null($_SESSION['user']['role'])) {
    header("location: " . BASE_URL);
  }
  // checks if logged in admin user can update post
  function canUpdatePost($post_id = null){
    global $conn;

    if(in_array(['permission_name' => 'update-post'], $_SESSION['userPermissions'])){
      if ($_SESSION['user']['role'] === "Author") { // author can update only posts that they themselves created
          $sql = "SELECT user_id FROM posts WHERE id=?";
          $post_result = getSingleRecord($sql, 'i', [$post_id]);
          $post_user_id = $post_result['user_id'];

          // if current user is the author of the post, then they can update the post
          if ($post_user_id === $user_id) {
            return true;
          } else { // if post is not created by this author
            return false;
          }
      } else { // if user is not author
        return true;
      }
    } else {
      return false;
    }
  }

  // accepts user id and post id and checks if user can publis/unpublish a post

  function canManageUser() {
    if(in_array(['permission_name' => 'manage-user'], $_SESSION['userPermissions'])){
      return true;
    } else {
      return false;
    }
  }

  function canManageRoles() {
    if(in_array(['permission_name' => 'manage-roles'], $_SESSION['userPermissions'])){
      return true;
    } else {
      return false;
    }
  }

  /* Explore Module Permissions */ 

  function canManageListing() {
    if(in_array(['permission_name' => 'manage-listing'], $_SESSION['userPermissions'])){
      return true;
    } else {
      return false;
    }
  }

  function canManageAreas() {
    if(in_array(['permission_name' => 'manage-areas'], $_SESSION['userPermissions'])){
      return true;
    } else {
      return false;
    }
  }

  function canManageCategories() {
    if(in_array(['permission_name' => 'manage-categories'], $_SESSION['userPermissions'])){
      return true;
    } else {
      return false;
    }
  }

  function canManageEvents() {
    if(in_array(['permission_name' => 'manage-events'], $_SESSION['userPermissions'])){
      return true;
    } else {
      return false;
    }
  }
  
?>
