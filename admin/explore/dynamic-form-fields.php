<?php include('../../config.php') ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php

$output = '';
$cuisine = isset($_POST['cuisine']) ? $_POST['cuisine'] : '';
$tyid = isset($_POST['type_id']) ? $_POST['type_id'] : '';
$delivery_time = isset($_POST['delivery_time']) ? $_POST['delivery_time'] : '';
$charges = isset($_POST['charges']) ? $_POST['charges'] : 0;


  if(!empty($_POST['c_id']))

{

if(!empty(getCategoryAllType($_POST['c_id']))) {

  $ctypes = getCategoryAllType($_POST['c_id']);
  
  $output = '<label class="control-label">Choose Type</label>
  <select name="type_id"  class="form-control" >';
  
  $output .= '<option value="">Select Type</option>';
  
  foreach($ctypes as $ctype) {
 
   $output .= '<option value = "'. $ctype['id']. '" '.(($tyid == $ctype["id"]) ? 'selected' : '' ).'  >'. $ctype['type_name'] . '</option>';
}

   $output .= '</select>';
}


if($_POST['c_id'] == 1)  {
  $output .= '<label  class="control-label" > Cuisine : </label> 
  <input type="text" value = "'.$cuisine.'" name="cuisine"  class="form-control" >';
  $output .= '<br><div class="form-group"><label  class="control-label" > Delivery Time (in Minutes) : </label>
  <input type="number" min="20" max="200" value = "'.$delivery_time.'" name="delivery_time" class="form-control"/>';
}

elseif($_POST['c_id'] ==2 ) {
  $output.='<br><label  class="control-label" > Average Charges (Per Month): </label> 
  <input type="text" name="charges" value= "'.$charges.'" class="form-control" >';
} 

}

echo $output;
?>