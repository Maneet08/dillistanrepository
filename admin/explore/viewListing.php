<?php include('..\..\config.php'); ?>
<?php include(INCLUDE_PATH . '\logic\common_functions.php') ?>
<?php include('./placeLogic.php') ?>

<?php $areas = getAreas(); 


foreach($areas as $key => $value) {
    $Restaurants = getRestaurants($value['id']);
    if(!empty($Restaurants))
    echo "<center><h1>Restaurants in ".$value['area_name']."</h1></center>";
    foreach($Restaurants as $Restaurant) :?>
     <div style="border: 1px solid black;padding:40px;margin:5px;display:inline-block;width:15%">
  <?php  echo "<img src ='../../assets/images/listings/".$Restaurant['name']."/".$Restaurant['feature_image']."' width='100%'/><br><br>
         Name : ".$Restaurant['name']."<br>
         Descripton : ".$Restaurant['description']."<br>
         Address : ".$Restaurant['address']."<br>
         Contact : ".$Restaurant['contact']."<br>
         Area : ".$value['area_name']."<br>
         Region : ".$Restaurant['region_name']."<br>
         Continental : ".$Restaurant['Cuisine']."<br>
         Delivery Time : ".$Restaurant['Delivery_Time']." Minutes<br>
         Open Time : ".$Restaurant['open_time']." <br>
         Close Time : ".$Restaurant['close_time']." <br>";

            ?>
  
</div> <?php endforeach;

$clubs = getPlaces($value['id'], 2);
    if(!empty($clubs))
    echo "<center><h1>Club & Bars in ".$value['area_name']."</h1></center>";
    foreach($clubs as $club) :?>
     <div style="border: 1px solid black;padding:40px;margin:5px;display:inline-block">
  <?php  echo "<img src ='../../assets/images/listings/".$club['name']."/".$club['feature_image']."' width='100%'/><br><br>
         Name : ".$club['name']."<br>
         Descripton : ".$club['description']."<br>
         Address : ".$club['address']."<br>
         Contact : ".$club['contact']."<br>
         Area : ".$value['area_name']."<br>
         Region : ".$club['region_name']."<br>
         Open Time : ".$club['open_time']." <br>
         Close Time : ".$club['close_time']." <br>";

            ?>
  
</div> <?php endforeach;

    $gyms = getGym($value['id']);
    if(!empty($gyms)) 
    echo "<center><h1>Gyms in ".$value['area_name']."</h1></center>";
    foreach($gyms as $gym) : ?> 
    <div style="border: 1px solid black;padding:40px;margin:5px;display:inline-block">
  <?php  echo "<img src ='../../assets/images/listings/".$gym['name']."/".$gym['feature_image']."' width='100%'/><br><br>
         Name : ".$gym['name']."<br>
         Type : ".$gym['type_name']."<br>
         Description : ".$gym['description']."<br>
         Address : ".$gym['address']."<br>
         Contact : ".$gym['contact']."<br>
         Area : ".$value['area_name']."<br>
         Region : ".$gym['region_name']."<br>
         Charges : ".$gym['Charges']."<br>
         Open Time : ".$gym['open_time']." <br>
         Close Time : ".$gym['close_time']." <br>";

            ?>
  
    </div> <?php endforeach;

$salons = getPlaces($value['id'], 4);
    if(!empty($salons))
    echo "<center><h1>Salons in ".$value['area_name']."</h1></center>";
    foreach($salons as $salon) :?>
     <div style="border: 1px solid black;padding:40px;margin:5px;display:inline-block">
  <?php  echo "<img src ='../../assets/images/listings/".$salon['name']."/".$salon['feature_image']."' width='100%'/><br><br>
         Name : ".$salon['name']."<br>
         Descripton : ".$salon['description']."<br>
         Address : ".$salon['address']."<br>
         Contact : ".$salon['contact']."<br>
         Area : ".$value['area_name']."<br>
         Region : ".$salon['region_name']."<br>
         Open Time : ".$salon['open_time']." <br>
         Close Time : ".$salon['close_time']." <br>";

            ?>
  
</div> <?php endforeach;

$ngos = getPlaces($value['id'], 5);
    if(!empty($ngos))
    echo "<center><h1>NGOs in ".$value['area_name']."</h1></center>";
    foreach($ngos as $ngo) :?>
     <div style="border: 1px solid black;padding:40px;margin:5px;display:inline-block">
  <?php  echo "<img src ='../../assets/images/listings/".$ngo['name']."/".$ngo['feature_image']."' width='100%'/><br><br>
         Name : ".$ngo['name']."<br>
         Descripton : ".$ngo['description']."<br>
         Address : ".$ngo['address']."<br>
         Contact : ".$ngo['contact']."<br>
         Area : ".$value['area_name']."<br>
         Region : ".$ngo['region_name']."<br>
         Open Time : ".$ngo['open_time']." <br>
         Close Time : ".$ngo['close_time']." <br>";

            ?>
  
</div> <?php endforeach;


    $petadoptions = getPlaces($value['id'], 6);
    if(!empty($petadoptions)) 
    echo "<center><h1>Pet Adoption in ".$value['area_name']."</h1></center>";
    foreach($petadoptions as $petadoption) :?>
  <div style="border: 1px solid black;padding:40px;margin:5px;display:inline-block">
  <?php  echo "<img src ='../../assets/images/listings/".$petadoption['name']."/".$petadoption['feature_image']."' width='100%'/><br><br>
         Name : ".$petadoption['name']."<br>
         Type : ".$petadoption['type_name']."<br>
         Description : ".$petadoption['description']."<br>
         Address : ".$petadoption['address']."<br>
         Contact : ".$petadoption['contact']."<br>
         Area : ".$value['area_name']."<br>
         Region : ".$petadoption['region_name']."<br>
         Open Time : ".$petadoption['open_time']." <br>
         Close Time : ".$petadoption['close_time']." <br>";

            ?>
  
</div> <?php endforeach;

$spas = getSpas($value['id']);
    if(!empty($spas)) 
    echo "<center><h1>Spas in ".$value['area_name']."</h1></center>";
    foreach($spas as $spa) :?>
  <div style="border: 1px solid black;padding:40px;margin:5px;display:inline-block">
  <?php  echo "<img src ='../../assets/images/listings/".$spa['name']."/".$spa['feature_image']."' width='100%'/><br><br>
         Name : ".$spa['name']."<br>
         Description : ".$spa['description']."<br>
         Address : ".$spa['address']."<br>
         Contact : ".$spa['contact']."<br>
         Area : ".$value['area_name']."<br>
         Region : ".$spa['region_name']."<br>
         Open Time : ".$spa['open_time']." <br>
         Close Time : ".$spa['close_time']." <br>";

            ?>
  
</div> <?php endforeach;


$hospitals = getPlaces($value['id'], 8);
    if(!empty($hospitals)) 
    echo "<center><h1>Hospitals in ".$value['area_name']."</h1></center>";
    foreach($hospitals as $hospital) :?>
  <div style="border: 1px solid black;padding:40px;margin:5px;display:inline-block">
  <?php  echo "<img src ='../../assets/images/listings/".$hospital['name']."/".$hospital['feature_image']."' width='100%'/><br><br>
         Name : ".$hospital['name']."<br>
         Type : ".$hospital['type_name']."<br>
         Description : ".$hospital['description']."<br>
         Address : ".$hospital['address']."<br>
         Contact : ".$hospital['contact']."<br>
         Area : ".$value['area_name']."<br>
         Region : ".$hospital['region_name']."<br>
         Open Time : ".$hospital['open_time']." <br>
         Close Time : ".$hospital['close_time']." <br>";

            ?>
  
</div> <?php endforeach;

$doctors = getPlaces($value['id'], 8);
    if(!empty($doctors)) 
    echo "<center><h1>Doctors in ".$value['area_name']."</h1></center>";
    foreach($doctors as $doctor) :?>
  <div style="border: 1px solid black;padding:40px;margin:5px;display:inline-block">
  <?php  echo "<img src ='../../assets/images/listings/".$doctor['name']."/".$doctor['feature_image']."' width='100%'/><br><br>
         Name : ".$doctor['name']."<br>
         Type : ".$doctor['type_name']."<br>
         Description : ".$doctor['description']."<br>
         Address : ".$doctor['address']."<br>
         Contact : ".$doctor['contact']."<br>
         Area : ".$value['area_name']."<br>
         Region : ".$doctor['region_name']."<br>
         Open Time : ".$doctor['open_time']." <br>
         Close Time : ".$doctor['close_time']." <br>";

            ?>
  
</div> <?php endforeach;

}

?>