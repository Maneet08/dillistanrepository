<?php
$isEditing = false;
$place_id = 0;
$errors = array();
$region_id = 0;
$area_id = 0;
$category_id = 0;
$name = '';
$address = '';
$contact = '';
$fimg = '';
$descripton = '';
$open_time = '';
$close_time = '';
$type=0;
$charges = 0;
$cuisine = "";
$delivery_time = 0;
$description = '';
$listingId = 0;


// Storing common fields data
  if(isset($_POST['save_place']) OR isset($_POST['update_place'])) {

    $region_id = $_POST['region_id'];
    $type=  isset($_POST['type_id']) ? $_POST['type_id'] : $type;
    $area_id = isset($_POST['area_id']) ? $_POST['area_id'] : $area_id;
    $category_id = $_POST['cat_id'];
    $name = $_POST['pname'];
    $address = $_POST['paddress'];
    $contact = $_POST['pnumber'];
    $description = $_POST['pdescription'];
    $open_time = $_POST['potime'];
    $close_time = $_POST['pctime'];
    $cuisine = isset($_POST['cuisine']) ? $_POST['cuisine'] : $cuisine;
    $delivery_time = isset($_POST['delivery_time']) ? $_POST['delivery_time'] : $delivery_time;
    $charges = isset($_POST['charges']) ? $_POST['charges'] : $charges;  
  }
     

  if(isset($_POST['save_place'])) {

    $errors = validatePlace($_POST, ['save_place']);
    if(count($errors) === 0) {

      
        // Save Method call based on category
    
    switch($_POST['cat_id']) {
       
        case "1" : saveRestaurant();
                 break;

        case "2" : saveGym();
                   break;       
                   
        case "3" : savePlace();
                   break;          

        default:   saveRest();    
    }
}
}

if(isset($_POST['update_place'])) {

    $errors = validatePlace($_POST, ['update_place']);
    if(count($errors) === 0) {

        switch($_POST['cat_id']) {
       
            case "1" : updateRestaurant($_POST['place_id']);
                     break;
    
            case "2" : updateGym($_POST['place_id']);
                       break; 
                       
            case "3" : updateGym();
                       break;           
    
            default:   updateRest($_POST['place_id']);    
        }
    }

}

// continue editting if there were errors


// ACTION: fetch place for editting
if(isset($_GET['edit_place'])) {
    $place_id = $_GET['edit_place'];
    editPlace($place_id);
}   

function savePlace() {
    global $region_id, $area_id, $category_id, $name, $address, $contact, $fimg, $description, $open_time, $close_time,$listingId;
    $sql = "INSERT INTO listings (region_id, category_id, name, address, contact, description, open_time, close_time, area_id) VALUES (?, ?, ?, ?, ?,?, ?, ?, ?)";
    modifyRecord($sql, 'iissssssi', [$region_id, $category_id, $name, $address, $contact, $description, $open_time, $close_time, $area_id]);
    $idquery = "SELECT listing_id FROM listings WHERE contact=?";
    $listingId = getSingleRecord($idquery, 's' , [$contact]);
    $sqlgallery = "INSERT INTO listings_gallery (listingId, image) VALUES (?,?)";
    mkdir(ROOT_PATH."/assets/images/".ROOT_LISTING."/".$listingId['listing_id']);
    mkdir(ROOT_PATH."/assets/images/".ROOT_LISTING."/".$listingId['listing_id']."/Feature Image");
    mkdir(ROOT_PATH."/assets/images/".ROOT_LISTING."/".$listingId['listing_id']."/Gallery_Image");
    $fimg = uploadPicture(LISTING_IMAGE,ROOT_LISTING."/".$listingId['listing_id']."/Feature Image");
    $gimgs = uploadMultiplePicture('pgimage',ROOT_LISTING."/".$listingId['listing_id']."/Gallery_Image");
    $Insertfimg = "UPDATE listings SET feature_image = ? WHERE listing_id = ?";
    modifyRecord($Insertfimg, 'si', [$fimg,$listingId['listing_id']]);

    
    foreach($gimgs as $key => $value) {
      modifyRecord($sqlgallery, 'is', [$listingId['listing_id'],$value]);
    } 
}

function updatePlace($pid) {
    global $region_id, $area_id, $category_id, $name, $address, $contact, $fimg, $description, $open_time, $close_time;
    $fimg = isset($_POST['prevFI']) && empty($_FILES[LISTING_IMAGE]['name']) ? $_POST['prevFI'] : uploadPicture(LISTING_IMAGE,ROOT_LISTING."/$pid");
    $gimgs = uploadMultiplePicture('pgimage',ROOT_LISTING."/$pid/Gallery_Image" );
    $sql = "UPDATE listings SET region_id  = ?, category_id = ?, name = ?, address =?, contact = ?, feature_image = ?, description = ?, open_time =?, close_time = ?, area_id = ? WHERE listing_id = ?";
    modifyRecord($sql, 'iisssssssii', [$region_id, $category_id, $name, $address, $contact, $fimg, $description, $open_time, $close_time, $area_id,$pid]);
    $sqlgallery = "INSERT INTO listings_gallery (listingId, image) VALUES (?,?)";
    
    foreach($gimgs as $key => $value) {
      modifyRecord($sqlgallery, 'is', [$pid,$value]);

    } 
}

function saveRestaurant() {
    global $listingId,$cuisine,$delivery_time;
    savePlace();
    $sql = "INSERT INTO listing_addon SET id = ?, Cuisine = ?, Delivery_Time = ?";
    $result =  modifyRecord($sql, "iss", [$listingId['listing_id'],$cuisine, $delivery_time]); 

    if($result){
        $_SESSION['success_msg'] = "Listing successfully added";
        header("location: placeList.php");
        exit(0);
      } else {
        $_SESSION['error_msg'] = "Something went wrong. Could not add Listing in Database";
      }

}

function updateRestaurant($pid) {
    global $cuisine, $delivery_time;
    updatePlace($pid);
    $sql = "UPDATE listing_addon SET Cuisine = ?, Delivery_Time = ? WHERE id = ?";
    $result = modifyRecord($sql, "ssi", [$cuisine, $delivery_time, $pid]); 

    if($result){
        $_SESSION['success_msg'] = "Listing successfully Updated";
        header("location: placeList.php");
        exit(0);
      } 

}

function saveGym() {
    global $listingId,$type,$charges;
    savePlace();    
    $sql = "INSERT INTO listing_addon SET id = ?, Charges = ?, type_id = ?";
    $result = modifyRecord($sql, "isi", [$listingId['listing_id'],$charges, $type]);

    if($result){
        $_SESSION['success_msg'] = "Listing successfully added";
        header("location: placeList.php");
        exit(0);
      } else {
        $_SESSION['error_msg'] = "Something went wrong. Could not add Listing in Database";
      }
}

function updateGym($pid) {
    global $type,$charges;
    updatePlace($pid);    
    $sql = "UPDATE listing_addon SET Charges = ?, type_id = ? WHERE id = ?";
    $result = modifyRecord($sql, "sii", [$charges, $type, $pid]);

    if($result){
        $_SESSION['success_msg'] = "Listing successfully Updated";
        header("location: placeList.php");
        exit(0);
      } 
}



function saveRest() {
    global $listingId,$type;
    savePlace();
    $sql = "INSERT INTO listing_addon SET id = ?, type_id = ?";
    $result = modifyRecord($sql, "ii", [$listingId['listing_id'],$type]);

    if($result){
        $_SESSION['success_msg'] = "Listing successfully added";
        header("location: placeList.php");
        exit(0);
      } else {
        $_SESSION['error_msg'] = "Something went wrong. Could not add Listing in Database";
      }
    
    
}

function updateRest($pid) {
    global $type;
    updatePlace($pid);
    $sql = "UPDATE listing_addon SET type_id = ? WHERE id = ?";
    $result = modifyRecord($sql, "ii", [$type,$pid]);

    if($result){
        $_SESSION['success_msg'] = "Listing successfully Updated";
        header("location: placeList.php");
        exit(0);
      } 
    
}

function editPlace($pid){
    global $region_id, $area_id, $category_id, $name, $address, $contact, $fimg, $description, $open_time, $close_time,$type;
    global $cuisine,$charges,$delivery_time,$isEditing;
  
    $isEditing = true;
    $sql = "SELECT l.*, la.* FROM listings l JOIN listing_addon la ON l.listing_id = la.id WHERE l.listing_id=?";
    $place = getSingleRecord($sql, 'i', [$pid]);
    $fimg = $place['feature_image'];
    $name = $place['name'];
    $region_id = $place['region_id'];
    $area_id = $place['area_id'];
    $category_id = $place['category_id'];
    $address = $place['address'];
    $contact = $place['contact'];
    $description = $place['description'];
    $open_time = $place['open_time'];
    $close_time = $place['close_time'];
    $isEditing = true;
    if($category_id != 1 && $category_id != 3 )  $type = $place['type_id'];
    if($category_id == 1) {
     $cuisine = $place['Cuisine'];
     $delivery_time = $place['Delivery_Time'];
    }
    if($category_id == 2 ){
        $charges = $place['Charges'];
    }
   
    
  }


function getRestaurants($area_id) {

    $sql = "SELECT c.listing_id, c.name, c.address, c.contact, c.feature_image, c.description, 
    c.close_time, c.open_time, area.area_name ,a.Cuisine, a.Delivery_Time, r.region_name, cat.name as cat_name FROM listings c 
    JOIN listing_addon a ON c.listing_id=a.id JOIN region r ON c.region_id = r.id JOIN category cat ON c.category_id = cat.id 
    JOIN area ON c.area_id = area.id  WHERE c.category_id = 1 AND c.area_id =?";
    $Restaurants = getMultipleRecords($sql, 'i', [$area_id]);
    return $Restaurants;

} 

function getPlaces($area_id,$cat_id) {

    $sql = "SELECT c.listing_id, c.name, c.address, c.contact, c.feature_image, c.description, 
    c.close_time, c.open_time, area.area_name , t.type_name, r.region_name, cat.name as cat_name FROM listings c JOIN listing_addon a ON c.listing_id=a.id 
    JOIN region r ON c.region_id = r.id JOIN cat_type t ON a.type_id = t.id JOIN category cat ON c.category_id = cat.id 
    JOIN area ON c.area_id = area.id WHERE c.category_id = ? AND c.area_id =?";
    $places = getMultipleRecords($sql, 'ii', [$cat_id,$area_id]);
    return $places;
}

function getGym($area_id) {

    $sql = "SELECT c.listing_id, c.name, c.address, c.contact, c.feature_image, c.description, 
    c.close_time, c.open_time, area.area_name, a.Charges, t.type_name, r.region_name, cat.name as cat_name FROM listings c 
    JOIN listing_addon a ON c.listing_id=a.id JOIN region r ON c.region_id = r.id JOIN cat_type t ON a.type_id = t.id 
    JOIN category cat ON c.category_id = cat.id JOIN area ON c.area_id = area.id WHERE c.category_id = 2 AND c.area_id =?";
    $gym = getMultipleRecords($sql, 'i', [$area_id]);
    return $gym;

} 

function getSpas($area_id) {
    $sql = "SELECT l.listing_id, l.name, l.address, l.contact, l.feature_image, l.description, 
    l.close_time, l.open_time, area.area_name, r.region_name, cat.name as cat_name FROM listings l JOIN region r ON l.region_id  = r.id 
    JOIN category cat ON l.category_id = cat.id JOIN area ON l.area_id = area.id  WHERE category_id = 3 AND area_id =?";
    $Spas = getMultipleRecords($sql, 'i', [$area_id]);
    return $Spas;

} 

function getListinggalleryByID($listing_id) {
  $imgnames = array();
  $sql = "SELECT image FROM listings_gallery WHERE listingId =?";
  $records = getMultipleRecords($sql, 'i', [$listing_id]);
  foreach($records as $record) {
   array_push($imgnames, $record['image']);
  }
  return $imgnames;
}

if (isset($_POST['lid'])) {
  saveGallery();
  }

  function saveGallery() {
    $pid=$_POST['lid'];
    $sql = "DELETE FROM listings_gallery WHERE listingId = ? AND image=?";
    $result = modifyRecord($sql, 'is', [$pid, $_POST['filename']]);
    
    if ($result) {
      $image = ROOT_PATH."/assets/images/listings/".$pid."/Gallery_Image/". $_POST['filename'];
      unlink($image);
     }

}
  
    

?>