<?php include('../../config.php'); ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php include('placeLogic.php'); ?>
<?php $regions = getRegions();
      $categories = getAllCategory(); 
?>
<!DOCTYPE html>
<html>
<head>
  
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custom styles -->
  <link rel="stylesheet" href="../../static/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
  <div class="col-md-8 col-md-offset-2">
    
      <form class="form" name="pform" method="post" enctype="multipart/form-data">
        <?php if ($isEditing === true): ?>
        <h1 class="text-center">Update Place</h1>
        <?php else: ?>
        <h1 class="text-center">Create Place</h1>
        <?php endif; ?>
        <hr><br />

        <?php if ($isEditing === true): ?>
        <input type="hidden" name="place_id" value="<?php echo $place_id ?>">
        <?php endif; ?>

        <div class="form-group <?php echo isset($errors['region_id']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Choose Region : </label> 
                    <select id="r_id"  name="region_id"  class="form-control">
                            <option value="">Select Region</option>
                            <?php foreach ($regions as $region): ?>
                            <option value="<?php echo $region['id']; ?>" <?php if($region['id'] == $region_id): echo "selected"; endif; ?>><?php echo $region['region_name']; ?></option>
                            <?php endforeach; ?>
                    </select>
                    <?php if (isset($errors['region_id'])): ?>
                <span class="help-block"><?php echo $errors['region_id'] ?></span>
              <?php endif; ?>
              </div>


              <div class="form-group <?php echo isset($errors['area_id']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Choose Area : </label> 
                    <select id="a"  name="area_id"  class="form-control" disabled>
                            
                    </select>
                    <?php if (isset($errors['area_id'])): ?>
                <span class="help-block"><?php echo $errors['area_id'] ?></span>
              <?php endif; ?>
              </div>  

              <div class="form-group" style="">
              <label  class="control-label" >Feature Image : </label>
              <?php if (!empty($fimg)): ?>
                <img src="<?php echo BASE_URL . '/assets/images/listings/'.$place_id.'/Feature Image/'. $fimg; ?>" id="profile_img" alt="Profile Image" style="width:100%" alt="">
                <input type="hidden" name="prevFI" value="<?php echo $fimg; ?>" >
              <?php else: ?>
                <img src="http://via.placeholder.com/1900x800" id="profile_img" style="width:100%" alt="">
              <?php endif; ?>
              <input type="file" name="pfimage" id="profile_input" value="" style="display: none;">
              
            </div>
              

              <div class="form-group <?php echo isset($errors['pname']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Name : </label> 
                    <input type="text" name="pname" value="<?php echo $name ?>"  class="form-control" >
                    <?php if (isset($errors['pname'])): ?>
                <span class="help-block"><?php echo $errors['pname'] ?></span>
              <?php endif; ?>
              </div>      
       

<div <?php if($isEditing)  : ?> style="display:none" <?php endif;?> class="form-group <?php echo isset($errors['cat_id']) ? 'has-error': '' ?>">
          <label class="control-label">Choose Category</label>
          <select id="c_id"  name="cat_id"  class="form-control">
                            <option value="">Select Category</option>
                            <?php foreach ($categories as $cat): ?>
                            <option value="<?php  echo $cat['id']; ?>" <?php  if($cat['id'] == $category_id): echo "selected"; endif; ?>><?php echo $cat['name']; ?></option>
                            <?php endforeach; ?>
                    </select>
          <?php if (isset($errors['cat_id'])): ?>
             <span class="help-block"><?php echo $errors['cat_id'] ?></span>
          <?php endif; ?>
        </div>

        <div class="form-group <?php echo isset($errors['cuisine']) ? 'has-error': '' ?>">
        <div id="cu_id"></div>
         
          <?php if (isset($errors['cuisine'])): ?>
             <span class="help-block"><?php echo $errors['cuisine'] ?></span>
          <?php endif; ?>
        </div>

        <div class="form-group <?php echo isset($errors['type_id']) ? 'has-error': '' ?>">
        <div id="t_id"></div>
          <?php if (isset($errors['type_id'])): ?>
             <span class="help-block"><?php echo $errors['type_id'] ?></span>
          <?php endif; ?>
        </div>

        <div class="form-group <?php echo isset($errors['pdescription']) ? 'has-error': '' ?>">
                    <label  class="control-label" > Description : </label> 
                    <textarea  name="pdescription"  class="form-control" ><?php echo $description;?></textarea>
                    <?php if (isset($errors['pdescription'])): ?>
             <span class="help-block"><?php echo $errors['pdescription'] ?></span>
          <?php endif; ?>    
                    
        </div> 

        <div class="form-group <?php echo isset($errors['paddress']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Address : </label> 
                    <textarea name="paddress"  class="form-control" ><?php echo $address;?></textarea>
                    <?php if (isset($errors['paddress'])): ?>
                <span class="help-block"><?php echo $errors['paddress'] ?></span>
              <?php endif; ?>
        </div>
        
        <div class="form-group <?php echo isset($errors['pnumber']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Contact No : </label> 
                    <input type="number" name="pnumber" value="<?php echo $contact;?>"  class="form-control" />
                    <?php if (isset($errors['pnumber'])): ?>
                <span class="help-block"><?php echo $errors['pnumber'] ?></span>
              <?php endif; ?>
        </div>

        <div class="form-group <?php echo isset($errors['potime']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Open Time : </label> 
                    <input type="time" value="<?php echo $open_time;?>" name="potime"  class="form-control" >
                    <?php if (isset($errors['potime'])): ?>
                <span class="help-block"><?php echo $errors['potime'] ?></span>
              <?php endif; ?>
        </div>

        <div class="form-group <?php echo isset($errors['pctime']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Close Time : </label> 
                    <input type="time" value="<?php echo $close_time;?>" name="pctime"  class="form-control" >
                    <?php if (isset($errors['pctime'])): ?>
                <span class="help-block"><?php echo $errors['pctime'] ?></span>
              <?php endif; ?>
        </div>

        <div class="form-group" style="">
              <label  class="control-label" >Gallery_Images : </label>     
              <input type="file" multiple="multiple" name="pgimage[]"  value=""  >
              
            </div>
        <div class="form-group">
          <?php if ($isEditing === true): ?>
            <button type="submit" name="update_place" class="btn btn-primary">Update Place</button>
          <?php else: ?>
            <button type="submit" name="save_place" class="btn btn-success">Save Place</button>
          <?php endif; ?>
        </div>
      </form>
  </div>
  <?php include(INCLUDE_PATH . '/layouts/footer.php') ?>
  <script>
   $(document).ready(function () {
      $("#r_id").change(function () {
          
          var r_id = $(this).val();
          $.ajax({
              url: "<?php echo BASE_URL; ?>admin/Area/areabyRegion.php",
              method: "POST",
              data:{r_id:r_id},
              success: function(data) {
                  $("#a").html(data);
                  document.pform.area_id.disabled=false;
              }
          });
      });


<?php if(!empty($region_id)) : ?>
$.ajax({
              url: "<?php echo BASE_URL; ?>admin/Area/areabyRegion.php",
              method: "POST",
     data:{ r_id:<?php echo $region_id ?> <?php if(!isset($errors['area_id'])) :?> , a_id : <?php echo $area_id ?> 
     <?php endif; ?>},
              success: function(data) {
                  $("#a").html(data);
                  document.pform.area_id.disabled=false;
              }
          });
        <?php endif; ?>

        <?php if(!empty($category_id)) : ?>
          $.ajax({
              url: "dynamic-form-fields.php",
              method: "POST",
          data:{c_id:<?php echo $category_id; ?>
          <?php if(isset($cuisine)) : ?>, cuisine:"<?php echo $cuisine; ?>"<?php endif;?>
          <?php if(isset($delivery_time)) : ?>, delivery_time:"<?php echo $delivery_time; ?>"<?php endif;?>
          <?php if(isset($type)) : ?>, type_id: "<?php echo $type; ?>"<?php endif;?>
          <?php if(isset($charges)) : ?>, charges: "<?php echo $charges; ?>"<?php endif;?>
          },
              success: function(data) {

                if(c_id != 1) {
                  $("#t_id").html(data);
                  $("#cu_id").html("");
                }
                else {
                  $("#cu_id").append(data);
                  $("#t_id").html("");
                }
              }
          });    
        <?php endif; ?>
        
      $("#c_id").change(function () {
        
          var c_id = $(this).val();
          $.ajax({
              url: "dynamic-form-fields.php",
              method: "POST",
              data:{c_id:c_id},
              success: function(data) {

                if(c_id != 1) {
                  $("#t_id").html(data);
                  $("#cu_id").html("");
                }
                else {
                  $("#cu_id").append(data);
                  $("#t_id").html("");
                }
              }
          });
      });

     



   } );
  </script>
  <script type="text/javascript" src="../../assets/js/display_profile_image.js"></script>

</body>
</html>


 