<?php include('../../config.php') ?>
<?php include('placeLogic.php') ?>
<?php
  if (isset($_GET['listing_id'])) {
    $listing_id = $_GET['listing_id']; 
    $gallery = getListinggalleryByID($listing_id); 
  }
?>
<!DOCTYPE html>
<html>
<head>
  
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../static/css/style.css">
  <style>
  .delete {
    cursor:pointer
  }
  </style>
</head>
<body style="margin: 5% 5% 5% 5%">  
 
  <div class="col-md-4 col-md-offset-4">
    <a href="placeList.php" class="btn btn-success">
      <span class="glyphicon glyphicon-chevron-left"></span>
      Listings
    </a>
    <hr>
    <h1 class="text-center">Edit Gallery</h1>
    <br />
    <?php if (count($gallery) > 0): ?>
      <table class="table table-bordered">
        <thead>
          <tr><th>Image name</th></tr>
        </thead>
        <tbody>
          <?php foreach ($gallery as $val): ?>
            <tr class="text-center">
              <td style="position:relative">
              <img onclick="del(event,'<?php echo $val ?>')" class="delete" style="position:absolute;right:0;top:0" src="https://upload.wikimedia.org/wikipedia/en/2/26/Disctemp-x.png" width="20"/>
              <img src="<?php echo BASE_URL."/assets/images/listings/".$listing_id."/Gallery_Image/".$val; ?>" width="100" />
              <input type="checkbox" style="display:none" name="gallery_images[]" value="<?php echo $val ?>" checked />
              </td>    
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h2 class="text-center">No Images in database</h2>
    <?php endif; ?>
  </div>
  <?php include(INCLUDE_PATH . '/layouts/footer.php') ?>
</body>
</html>
<script>
function del(event , file) {
  if(confirm('You really want to delete this ?!!' )) {
event.target.parentNode.parentNode.remove();

$.ajax({
              url: "editGallery.php",
              method: "POST",
              data: {filename: file, lid: <?php echo $listing_id; ?>}
             
});

if($("td").length < 1) {
  $("h1").html("No Images in database");
  $("form").html("");
}


}}

</script>