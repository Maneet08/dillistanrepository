<?php include('../../../config.php') ?>
<?php include('categoryLogic.php') ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php
  $types = getAllCatType();
  if (isset($_GET['assign_type'])) {
    $cat_id = $_GET['assign_type']; // The ID of the Category whose Types we are changing
    $cat_types = getCategoryAllType($cat_id); // Getting all types belonging to category

    // array of permissions id belonging to the role
    $c_types_id = array_column($cat_types, "id");
  }
?>
<!DOCTYPE html>
<html>
<head>
  
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../static/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">  
 
  <div class="col-md-4 col-md-offset-4">
    <a href="categoryList.php" class="btn btn-success">
      <span class="glyphicon glyphicon-chevron-left"></span>
      Categories
    </a>
    <hr>
    <h1 class="text-center">Assign Type</h1>
    <br />
    <?php if (count($types) > 0): ?>
    <form  method="post">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>S.No.</th>
            <th>Type</th>
            <th class="text-center">Status</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($types as $key => $value): ?>
            <tr class="text-center">
              <td><?php echo $key + 1; ?></td>
              <td><?php echo $value['type_name']; ?></td>
              <td>
                  <input type="hidden" name="cat_id" value="<?php echo $cat_id; ?>">
                  <!-- if current type id is inside category's ids, then check it as already belonging to category -->
                  <?php if (in_array($value['id'], $c_types_id)): ?>
                    <input type="checkbox" name="type[]" value="<?php echo $value['id'] ?>" checked>
                  <?php else: ?>
                    <input type="checkbox" name="type[]" value="<?php echo $value['id'] ?>" >
                  <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
          <tr>
            <td colspan="3">
              <button type="submit" name="save_types" class="btn btn-block btn-success">Save Types</button>
            </td>
          </tr>
        </tbody>
      </table>
    </form>
    <?php else: ?>
      <h2 class="text-center">No Types in database</h2>
    <?php endif; ?>
  </div>
  <?php include(INCLUDE_PATH . '/layouts/footer.php') ?>
</body>
</html>