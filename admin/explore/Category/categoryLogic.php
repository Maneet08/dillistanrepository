<?php
  $cat_id = 0;
  $name = "";
  $description = "";
  $isEditting = false;
  $category = array();
  $errors = array();
 

  // ACTION: update category
  if (isset($_POST['update_cat'])) {
      $cat_id = $_POST['cat_id'];
      updateCat($cat_id);
  }
  // ACTION: Save Category
  if (isset($_POST['save_cat'])) {
      saveCat();
  }
  // ACTION: fetch Category for editting
  if (isset($_GET["edit_cat"])) {
    $cat_id = $_GET['edit_cat'];
    editCat($cat_id);
  }
  // ACTION: Delete Category
  if (isset($_GET['delete_cat'])) {
    $cat_id = $_GET['delete_cat'];
    deleteCat($cat_id);
  }
  // Save Category to database
  function saveCat(){
    global $errors, $name, $description;
    $errors = validateCat($_POST, ['save_cat']);
    if (count($errors) === 0) {
       // receive form values
       $name = $_POST['name'];
       $sql = "INSERT INTO category SET name=?";
       $result = modifyRecord($sql, 's', [$name]);

       if ($result) {
         $_SESSION['success_msg'] = "Category created successfully";
         header("location: categoryList.php");
         exit(0);
       } else {
         $_SESSION['error_msg'] = "Something went wrong. Could not save category in Database";
       }
    }
  }
  function updateCat($cat_id){
    global $errors, $name, $isEditting; // pull in global form variables into function
   
    $errors = validateCat($_POST, ['update_cat']);
    if (count($errors) === 0) {
      // receive form values
      $name = $_POST['name'];
      $sql = "UPDATE category SET name=? WHERE id=?";
      $result = modifyRecord($sql, 'si', [$name, $cat_id]);

      if ($result) {
        $_SESSION['success_msg'] = "Category successfully updated";
        $isEditting = false;
        header("location: categoryList.php");
        exit(0);
      } else {
        $_SESSION['error_msg'] = "Something went wrong. Could not save category in Database";
      }
    }
  }
  function editCat($cat_id){
    global $name, $isEditting;
    $sql = "SELECT * FROM category WHERE id=? LIMIT 1";
    $cat = getSingleRecord($sql, 'i', [$cat_id]);

    $cat_id = $cat['id'];
    $name = $cat['name'];
    $isEditting = true;
  }
  function deleteCat($cat_id) {
    $sql = "DELETE FROM category WHERE id=?";
    $result = modifyRecord($sql, 'i', [$cat_id]);
    if ($result) {
      $_SESSION['success_msg'] = "Category trashed!!";
      header("location: " . BASE_URL . "admin/explore/categoryList.php");
      exit(0);
    }
  }
  


 // ... other functions up here
  //ACTION SAVE Types
// Remember 'save_types' is the name of the submit button

if (isset($_POST['save_types'])) {
  saveCatTypes();
  }

  function saveCatTypes() {
    global $cat_id;
    /* add this as hidden input in assignPermissions.php
    simply <input type="hidden" name="cat_id" value="&lt;?php echo $cat_id ;?&gt;"> in the form tags*/
    $cat_id=$_POST['cat_id'];
    
    $sql = "DELETE FROM cat_type_map WHERE cat_id=?";
    $result = modifyRecord($sql, 'i', [$cat_id]);
    
    if ($result) {
    foreach ($_POST['type'] as $id) { //a little change also here
    $sql_2 = "INSERT INTO cat_type_map SET cat_id=?, type_id=?";
    modifyRecord($sql_2, 'ii', [$cat_id, $id]);
    }

    }
    
    $_SESSION['success_msg'] = "Types saved";
    header("location: categoryList.php");
    exit(0);
    }