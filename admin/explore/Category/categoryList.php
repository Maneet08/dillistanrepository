<?php include('../../../config.php') ?>
<?php include('categoryLogic.php') ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php
  $category = getAllCategory();
?>
<!DOCTYPE html>
<html>
<head>
 
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../static/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
  
  <div class="col-md-8 col-md-offset-2">
  <?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
    <h1 class="text-center">Listing Categories</h1>
    <hr><br />
    <?php if (isset($category)): ?>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>S. No.</th>
            <th>Category name</th>
            <th colspan="3" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($category as $key => $value): ?>
            <tr>
              <td><?php echo $key + 1; ?></td>
              <td><?php echo $value['name'] ?></td>
              <td class="text-center">
                <a href="assignType.php?assign_type=<?php echo $value['id'] ?>" class="btn btn-sm btn-info">
                  Types
                </a>
              </td>
              <td class="text-center">
                <a href="categoryForm.php?edit_cat=<?php echo $value['id'] ?>" class="btn btn-sm btn-success">
                  <span class="glyphicon glyphicon-pencil"></span>
                </a>
              </td>
              <td class="text-center">
                <a href="<?php echo BASE_URL ?>admin/explore/categoryForm.php?delete_cat=<?php echo $value['id'] ?>" class="btn btn-sm btn-danger">
                  <span class="glyphicon glyphicon-trash"></span>
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h2 class="text-center">No category in database</h2>
    <?php endif; ?>
  </div>
  <?php include(INCLUDE_PATH . '/layouts/footer.php') ?>
</body>
</html>