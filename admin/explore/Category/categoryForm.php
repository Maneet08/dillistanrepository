<?php include('../../../config.php') ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php include('categoryLogic.php') ?>
<!DOCTYPE html>
<html>
<head>
  
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custom styles -->
  <link rel="stylesheet" href="../../static/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
  <div class="col-md-8 col-md-offset-2">
    
      <form class="form"  method="post">
        <?php if ($isEditting === true): ?>
          <h1 class="text-center">Update Category</h1>
        <?php else: ?>
          <h1 class="text-center">Create Category</h1>
        <?php endif; ?>
        <hr><br />

        <?php if ($isEditting === true): ?>
          <input type="hidden" name="cat_id" value="<?php echo $cat_id ?>">
        <?php endif; ?>
        <div class="form-group <?php echo isset($errors['name']) ? 'has-error': '' ?>">
          <label class="control-label">Category name</label>
          <input type="text" name="name" value="<?php echo $name; ?>" class="form-control">
          <?php if (isset($errors['name'])): ?>
            <span class="help-block"><?php echo $errors['name'] ?></span>
          <?php endif; ?>
        </div>

        <div class="form-group">
          <?php if ($isEditting === true): ?>
            <button type="submit" name="update_cat" class="btn btn-primary">Update Category</button>
          <?php else: ?>
            <button type="submit" name="save_cat" class="btn btn-success">Save Category</button>
          <?php endif; ?>
        </div>
      </form>
  </div>
  <?php include(INCLUDE_PATH . '/layouts/footer.php') ?>
</body>
</html>