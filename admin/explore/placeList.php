<?php include('../../config.php') ?>
<?php include('placeLogic.php') ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php
  $areas = getAreas();
  $listings = [];
  foreach($areas as $area) {
  if(count(getRestaurants($area['id'])) > 0)    
  array_push($listings,getRestaurants($area['id']));
  if(count(getGym($area['id'])) > 0) 
  array_push($listings,getGym($area['id']));
  if(count(getSpas($area['id'])) > 0) 
  array_push($listings,getSpas($area['id']));
 
  for($i=4; $i < count(getAllCategory()); $i++) {
    if(count(getPlaces($area['id'], $i)) > 0)  
    array_push($listings,getPlaces($area['id'], $i));   
  }

}

?>
<!DOCTYPE html>
<html>
<head>
 
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../static/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
  
  <div class="col-md-8 col-md-offset-2">
  <?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
    <h1 class="text-center">Listings</h1>
    <hr><br />
    <?php if (isset($listings)): ?>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>S. No.</th>
            <th>Listing name</th>
            <th>Listing Category</th>
            <th>Listing Region</th>
            <th>Listing Area</th>
            <th colspan="3" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=0; foreach ($listings as $listing): ?>
          <?php foreach ($listing as $key => $value) : ?>
            <tr>
              <td><?php echo ++$i; ?></td>
              <td><?php echo $value['name'] ?></td>
              <td><?php echo $value['cat_name'] ?></td>
              <td><?php echo $value['region_name'] ?></td>
              <td><?php echo $value['area_name'] ?></td>
              
              <td class="text-center">
                <a href="editGallery.php?listing_id=<?php echo $value['listing_id'] ?>" class="btn btn-sm btn-success">
                  <span class="glyphicon glyphicon-camera"></span>
                </a>
              </td>
              <td class="text-center">
                <a href="placeForm.php?edit_place=<?php echo $value['listing_id'] ?>" class="btn btn-sm btn-success">
                  <span class="glyphicon glyphicon-pencil"></span>
                </a>
              </td>
              <td class="text-center">
                <a href="placeForm.php?delete_listing=<?php echo $value['listing_id'] ?>" class="btn btn-sm btn-danger">
                  <span class="glyphicon glyphicon-trash"></span>
                </a>
              </td>
            </tr>
            <?php endforeach; ?> 
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h2 class="text-center">No Listing Found</h2>
    <?php endif; ?>
  </div>
  <?php include(INCLUDE_PATH . '/layouts/footer.php') ?>
</body>
</html>