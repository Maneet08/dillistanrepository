<?php
$isEditing = false;
$cid= 0;
$name = '';

if(isset($_GET['category'])) {
    $isEditing = true;
    getName( $_GET['category']);
}

if(isset($_POST['save_eventCat'])) {
    $name= $_POST['eCatname'];
    $addCat = "INSERT INTO events_cat SET Name= ?";
    if(modifyRecord($addCat, 's', [$name])) {
        $_SESSION['success_msg'] = "New Category Added";
    }

    else {
        $_SESSION['error_msg'] = "Couldn't Add Category. Please try Again";
    }
}

if(isset($_POST['update_eventCat'])) {
    $name= $_POST['eCatname'];
    $updateCat = "UPDATE events_cat SET Name= ? WHERE ID = ?";
    if(modifyRecord($updateCat, 'si', [$name,$_POST['id']])) {
        $_SESSION['success_msg'] = "Category Update";
    }
    
    else {
        $_SESSION['error_msg'] = "Couldn't Update Category. Please try Again";
    }

}

function getName($id) {
    global $name;
    $getName = "SELECT Name FROM events_cat WHERE ID = ?";
    $runQuery = getSingleRecord($getName, 'i', [$id]);
    $name = $runQuery['Name'];   
}


?>