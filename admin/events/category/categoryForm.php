<?php include('../../../config.php'); ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php include('categoryLogic.php'); ?>
<html>
<head>
  
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custom styles -->
  <link rel="stylesheet" href="../../static/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
<?php if ($isEditing): ?>
    <h1 class="text-center">Update Category</h1>
        <?php else: ?>
        <h1 class="text-center">Create Category</h1>
        <?php endif; ?>
        <hr><br />
        <form class="form" name="pform" method="post" enctype="multipart/form-data">
<?php if ($isEditing): ?><input type="hidden" name="id" value="<?php echo ($_GET['category']) ?  $_GET['category'] : '' ?>"/> <?php endif; ?>
        <div class="form-group <?php //echo isset($errors['eCatname']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Name : </label> 
                    <input type="text" name="eCatname" value="<?php echo $name ?>"  class="form-control" >
                    <?php //if (isset($errors['eCatname'])): ?>
                <span class="help-block"><?php //echo $errors['eCatname'] ?></span>
              <?php //endif; ?>
        </div>
        <div class="form-group">
          <?php if ($isEditing): ?>
           <button class="form-control" type="submit" name="update_eventCat" class="btn btn-primary">Update Category</button>
          <?php else: ?>
            <button class="form-control" type="submit" name="save_eventCat" class="btn btn-success">Save Category</button>
          <?php endif; ?>
        </div>
        </form>

</body>

</html>