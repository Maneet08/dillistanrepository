<?php include('../../config.php') ?>
<?php include("eventLogic.php") ?>

<html>
<head>
<title><?php echo $event['title'] ?></title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custom styles -->
  <link rel="stylesheet" href="../../static/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
<div class="row" style="padding: 10%">
<center>
<h1><?php echo $event['title'] ?></h1>
<form action="eventBooking.php" class="form" method="post" id="eventBook">
<input type="hidden" name = "eventID" value="<?php echo $_GET['event']?>" />
<input type="hidden" name ="eventTitle" value="<?php echo $event['title']?>" />
<div class="form-group" >
<input type="number" onchange="checkSeatavailability()" name="seats" value="1" min="0" max="<?php echo $event['seats'] ?>" required>
</div>
<div class="form-group" >
<label  class="control-label" >Rate:</label> <b><?php echo $event['price_per_seat'] ?></b> Rs.
</div>
<div class="form-group" >
<label  class="control-label" >You save:</label> <b><span id="discount">0</span></b> %
</div>
<div class="form-group">
<label  class="control-label" >Amount :</label>  
<b><span class="price"><?php echo $event['price_per_seat'] ?></span></b> Rs.
</div>

<div class="form-group">
<input type="submit"  value="Book Your Tickets" class="btn btn-success btn-block"  />
</div>
</form>
</center>
</div>
<?php include(INCLUDE_PATH. '/layouts/footer.php') ; ?>
<script>
rate = price = <?php echo $event['price_per_seat'] ?>;
off = 0;
var upperLimit = 0;
var lowerLimit = 0;
var discount = 0;


function setPrice() {
  if($("input[name='seats']").val()) {
  seats = parseInt($("input[name='seats']").val());
  
  if(!(seats < lowerLimit  || seats > upperLimit)) {  
      updatePrice(discount, seats);
     }
else {
    $.ajax({
    url: "discountedrate.php",
    method: "POST",
    data: {seats : seats , tableName : "<?php echo $event['price_slab'] ?>" },
    success : function (dataobject) {
      data = JSON.parse(dataobject);
      upperLimit = data.upperLimit;
      lowerLimit = data.lowerLimit;
      discount = data.discount;
      updatePrice(discount, seats);
    }
  })
  
}

}}

function updatePrice(discount, seats) {
off = discount;  
price = (seats * rate)*(1 - (discount/100))  ;
$("#discount").html(discount);
$("input[name='discount']").val(discount);
$(".price").html(price.toFixed(2));
}


function checkSeatavailability() {
  $.ajax ({
    url : "getSeatsAvailable.php",
    method : "POST",
    data : {id : <?php echo $event['ID'] ?>},
    success : function(data) {
      if(data && data < parseInt($("input[name='seats']").val())) {
     alert("Only "+data+" seats are available.");
      $("input[name='seats']").val(data);
      }
      setPrice();
    } 
  });
  
  }


$("#eventBook").submit( function(eventObj) {
      $("<input />").attr("type", "hidden")
          .attr("name", "discount")
          .attr("value", off)
          .appendTo("#eventBook");

      $("<input />").attr("type", "hidden")
        .attr("name", "tariff")
        .attr("value", <?php echo $event['price_per_seat'] ?>)
        .appendTo("#eventBook");    
      return true;
  });

  checkSeatavailability();
</script>
</body>
</html>