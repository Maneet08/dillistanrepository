<?php include('../../config.php') ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php 
if($_POST) {

$id = $_POST['bookingId'];
$seatsToCancel = $_POST['seatsToCancel'];

$sql = "UPDATE event_booking SET booking_status = ?, tickets_cancelled = ? WHERE ID =?";
modifyRecord($sql, 'sii', ["admin cancellation approval pending", $seatsToCancel ,$id]);

}
?>
<?php $bookings = getBookingsforUser($_SESSION['user']['id']) ;   ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Admin Area - Users </title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
<div class="col-md-12">
  <?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
<h1 class="text-center">Bookings</h1>
<hr/>
<br>
<?php if($bookings) : ?>
<table class="table table-bordered">
<thead>
<th>Title</th>
<th>Category</th>
<th colspan="1">Date</th>
<th>Seats</th>
<th>Price</th>
<th>Status</th>
<th>Payment ID </th>
<th>Action</th>

</thead>
<tbody>
<?php foreach($bookings as $booking) :?>
<tr>
<td><?php echo $booking['title']?></td>
<td><?php echo $booking['Name']?></td>
<td colspan="1"><?php echo $booking['booking_date']?></td>
<td><?php echo $booking['tickets_booked']?></td>
<td>₹ <?php echo $booking['total_amount']?></td>
<td class= "Status"><?php echo $booking["booking_status"] ?></td>
<td><?php echo $booking['payment_id']?></td>
<?php if($booking["booking_status"] == "booked") :?>
<td>
    <form action="" method="POST" id="<?php echo $booking['ID'] ?>">
        <input type="hidden" name="bookingId" value="<?php echo $booking['ID'] ?>" />
        <input type="hidden" name="seatsToCancel" />
        <input type="submit" class="btn btn-sm btn-danger" onclick="requestCancellation(<?php echo $booking['ID'].",".$booking['tickets_booked'] ?>,event)" value = "Cancel" />
    </form>
</td> 
<?php endif; ?>
</tr>
<?php endforeach; ?>
</tbody>
</table>
<div style="width:100%;height:100%;background:rgba(0,0,0,0.6);position:absolute;top:0;left:0;display:none;justify-content:center;align-items:center" id="modal" >
    
    <div style="width:30%;height:150px;background:white;display:flex;justify-content:space-evenly;align-items:center;flex-wrap:wrap">
    <div>Cancel</div>
    <div id="seatCancelDropdown"></div>
    <span>Seats / Bookings</span>
    <div>
   <button class="btn btn-success" id="submit">Submit</button>
<button class="btn btn-danger" id="cancel">Cancel</button>
    </div>
    </div>
   
</div>
<?php else : ?>
<h2 class="text-center">No Booking Found</h2>
<?php endif; ?>
</div>
<?php include(INCLUDE_PATH. '/layouts/footer.php') ; ?>
<script>

function requestCancellation(id,seats,event) {
        event.preventDefault();
        
        dropdown = "<select name='cancelledSeats'><option value='"+seats+"'>All</option>";
        for(i=1;i<seats;i++) {
            dropdown += "<option value='"+i+"'>"+i+"</option>"
        }
        dropdown += "</select>";
        $("#seatCancelDropdown").html(dropdown);
        $("#modal").css("display", "flex");
        
        
        $("button.btn").click(function() {
            $("#modal").css("display", "none");
        });
        
        $("button.btn-success").click(function() {
           
           if(confirm("Do you really want to cancel you tickets ?")) {
           $("input[name='seatsToCancel']").val($("select[name='cancelledSeats']").val());
           $("form#"+id).submit();
           }
        
        });
        
}
</script>
</body>
</html>

