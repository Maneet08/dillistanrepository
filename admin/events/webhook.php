<?php
include('../../config.php');
/*
Basic PHP script to handle Instamojo RAP webhook.
*/

$data = $_POST;
$mac_provided = $data['mac'];  // Get the MAC from the POST data
unset($data['mac']);  // Remove the MAC key from the data.
$ver = explode('.', phpversion());
$major = (int) $ver[0];
$minor = (int) $ver[1];
if($major >= 5 and $minor >= 4){
     ksort($data, SORT_STRING | SORT_FLAG_CASE);
}
else{
     uksort($data, 'strcasecmp');
}
// You can get the 'salt' from Instamojo's developers page(make sure to log in first): https://www.instamojo.com/developers
// Pass the 'salt' without <>
$mac_calculated = hash_hmac("sha1", implode("|", $data), "beb01cbafddc4ce4830ebedf37742e09");
if($mac_provided == $mac_calculated){
    if($data['status'] == "Credit"){
        $sql  = "SELECT seats from events WHERE ID = ?";
        $total_seats = getSingleRecord($sql , 'i', [$_GET['eventId']]);
        $sql = "UPDATE events SET seats = ? WHERE ID = ?";
        modifyRecord($sql , 'si', [$total_seats['seats'] - $_GET['seats'],$_GET['eventId'] ] );

        $sql = "UPDATE event_booking SET booking_status = ?, payment_id = ? WHERE ID = ?";
        modifyRecord($sql, 'ssi', ['booked',$_POST['payment_id'], $_GET['bookingId']]);
    }
    else{
        $sql = "UPDATE event_booking SET booking_status = ? WHERE ID = ?";
        modifyRecord($sql, 'ssi', ['failed', $_GET['bookingId']]);
    }
}
else{
    echo "MAC mismatch";
}
?>