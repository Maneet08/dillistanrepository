<?php include('../../config.php') ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>

<?php
  $user_id = $_SESSION['user']['id'];
  $events = isSuperAdmin() ? getEvents() : getEventsbyUserId($user_id);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Admin Area - Users </title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">

  <div class="col-md-8 col-md-offset-2">
  <?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
<h1 class="text-center">Events</h1>
<hr/>
<?php if($events) :?>
<table class="table table-bordered">
<thead>
<th>Title</th>
<?php if(isSuperAdmin($user_id)) :?>
<th>Author</th>
<?php endif; ?>
<th>Image</th>
<th>Venue</th>
<th>Seats</th>
<th colspan="1" class="text-center">Action</th>
</thead>
<tbody>
<?php foreach($events as $event) : ?>
<tr>
<td>
<?php echo $event["title"]; ?>
</td>
<?php if(isSuperAdmin($user_id)) :?>
<td><?php echo $event['username'] ?></td>
<?php endif; ?>
<td>
<img src="<?php echo BASE_URL?>assets/images/events/<?php echo $event["image"]; ?>" height="100" />
</td>
<td>
<?php echo $event["address"]; ?>
</td>
<td>
<?php echo $event["seats"]; ?>
</td>
<td class="text-center"><a href="#" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-pencil"></span></a>
<span class="btn btn-sm btn-danger">
                  <span class="glyphicon glyphicon-trash"></span></span></td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php else : ?>
<h2 class="text-center">No Event Found</h2>
<?php endif; ?>
<?php include(INCLUDE_PATH. '/layouts/footer.php') ; ?>
</body>
</html>