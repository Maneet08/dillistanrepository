<?php include('../../config.php') ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php 
if($_POST) {

if(isset($_POST['approve'])) {
    
$sql = "SELECT seats, price_per_seat FROM events WHERE ID = ?";
$EventData = getSingleRecord($sql, "i", [$_POST['eventId']]);
$refundAmount = ($_POST['cancelledSeats'] * $EventData['price_per_seat']) * (1 - ($_POST['discountAvailed'] / 100));



$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://test.instamojo.com/api/1.1/refunds/');
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
curl_setopt($ch, CURLOPT_HTTPHEADER,array("X-Api-Key:test_45ceb525267cb28d93239379388","X-Auth-Token:test_504c434698f3075e7b048702495"));

$payload = Array(
    'transaction_id'=> $_POST['paymentId'],
    'payment_id' => $_POST['paymentId'],
    'type' => 'QFL',
    'refund_amount' => $refundAmount, 
    'body' => "Customer isn't satisfied with the quality"
);

curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
$response = curl_exec($ch);
curl_close($ch); 

$responseArray = json_decode($response, true);


$sql = "UPDATE event_booking SET booking_status = 'cancelled', refund_status = ?, refund_amount = ? WHERE ID = ?";
modifyRecord($sql, 'sdi', [$responseArray['refund']['status'], $refundAmount, $_POST['bId']]);

$finalSeats = $EventData['seats'] + $_POST['cancelledSeats'];

$sql = "UPDATE events SET seats = ? WHERE ID = ?";
modifyRecord($sql, "ii" ,[$finalSeats, $_POST['eventId']]);
}
else {
    $sql = "UPDATE event_booking SET booking_status = 'booked' WHERE ID = ?";
    modifyRecord($sql, 'i', [$_POST['bId']]);
    
}

}
?>
<?php $cancelledBookings = isSuperAdmin() ? getCancelledBookingsforSuperAdmin() : getCancelledBookingsforAdmin($_SESSION['user']['id']) ;   ?>




<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Admin Area - Users </title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
<div class="col-md-12">
  <?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
<h1 class="text-center">Cancelled /  Request for Cancellation Bookings</h1>
<hr/>
<br>
<?php if($cancelledBookings) : ?>
<table class="table table-bordered">
<thead>
<th>Title</th>
<th>Category</th>
<th>Author</th>
<th>Customer Name</th>
<th>Customer Email</th>
<th colspan="1">Date</th>
<th>Seats</th>
<th>Price</th>
<th colspan="3">Action</th>

</thead>
<tbody>
<?php foreach($cancelledBookings as $cancelledbooking) :?>
<tr>
<td><?php echo $cancelledbooking['title']?></td>
<td><?php echo $cancelledbooking['Name']?></td>
<td><?php echo $cancelledbooking['author']?></td>
<td><?php echo $cancelledbooking['username']?></td>
<td><?php echo $cancelledbooking['email']?></td>
<td colspan="1"><?php echo $cancelledbooking['booking_date']?></td>
<td><?php echo $cancelledbooking['tickets_booked']?></td>
<td>₹ <?php echo $cancelledbooking['total_amount']?></td>
<?php if($cancelledbooking['booking_status'] == "admin cancellation approval pending") : ?>
<td><form action="" method="POST" >
    <input type="hidden" name="bId" value="<?php echo $cancelledbooking['ID'] ?>" />
    <input type="hidden" name="paymentId" value="<?php echo $cancelledbooking['payment_id'] ?>" />
    <input type="hidden" name="eventId" value="<?php echo $cancelledbooking['event_id'] ?>" />
    <input type="hidden" name="cancelledSeats" value="<?php echo $cancelledbooking['tickets_cancelled'] ?>" />
    <input type="hidden" name="discountAvailed" value="<?php echo $cancelledbooking['discount_availed'] ?>" />
    <input type="submit" name="approve" value="Approve" class="btn btn-sm btn-success"></input>
</a></form></td>
<td><form action="" method="POST" ><input type="hidden" name="bId" value="<?php echo $cancelledbooking['ID'] ?>" /><input type="submit" value="Decline"  class="btn btn-sm btn-danger"></input>
</a></form></td>
<?php endif;?>
</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php else : ?>
<h2 class="text-center">No Booking Found</h2>
<?php endif; ?>
</div>
<?php include(INCLUDE_PATH. '/layouts/footer.php') ; ?>
</body>
</html>
