<?php include('../../config.php'); ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>

<?php $events = getEvents();  ?>
<html>
<head>
  <meta charset="utf-8">
  <title>Admin Area - Users </title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
<div class="row">
<?php
foreach($events as $event) : ?>
     <div class="col-sm-3" style="border: 1px solid black;padding:40px;margin:5px;display:inline-block;">
  <?php  echo "<center><img src ='../../assets/images/events/".$event['image']."' style='width:50%' /></center><br><br>
         Name : ".$event['title']."<br>
         Descripton : ".$event['description']."<br>
         Address : ".$event['address']."<br>
         Area : ".$event['area_name']."<br>
         From : ".$event['start_date']." <br>
         Till : ".$event['end_date']." <br>
         Bookings Left : ".$event['seats']." <br>
         <a href='#' class='btn btn-sm btn-danger'>Book Tickets</a>";       ?>
  
</div> <?php endforeach; ?>
</div>
</body>
</html>