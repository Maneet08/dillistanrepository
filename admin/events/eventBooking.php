<?php include('../../config.php') ?>
<?php 

if($_POST) {
    $user_id  = $_SESSION['user']['id'];
    $sql = "SELECT * FROM users WHERE id=?";
    $user = getSingleRecord($sql, 'i', [$user_id]);
    
    $discount = $_POST['discount'];
    $tariff = $_POST['tariff'];
    $event_id = $_POST['eventID'];
    $event_title = $_POST['eventTitle'];
    $seats = $_POST['seats'];
    $currentDate =  date('Y-m-d');
    $time = date("h:i:sa");
    $status = 'pending';
    $total_price = ($seats * $tariff)*(1 - ($discount/100));

   $sql = "INSERT INTO event_booking(event_id,user_id, booking_date, time, tickets_booked,total_amount, discount_availed, booking_status) VALUES(?,?,?,?,?,?,?,?)";
   modifyRecord($sql, 'iissidds', [$event_id,$user_id,$currentDate,$time,$seats,$total_price,$discount,$status ]);   
   
   $sql = "SELECT ID FROM event_booking WHERE event_id = ? AND user_id = ? AND booking_date = ? AND  time = ? AND booking_status = ?";
   $id = getSingleRecord($sql, 'iisss', [$event_id,$user_id,$currentDate,$time,$status]);
   $bookingID = $id['ID'];
   ?>


<?php

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://test.instamojo.com/api/1.1/payment-requests/');
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
curl_setopt($ch, CURLOPT_HTTPHEADER,
            array("X-Api-Key:test_45ceb525267cb28d93239379388",
                  "X-Auth-Token:test_504c434698f3075e7b048702495"));
$payload = Array(
    'purpose' => $_POST['eventTitle']." (".$seats." tickets)",
    'amount' => $total_price,
    'phone' => '9999999999',
    'buyer_name' => $_SESSION["user"]["username"],
    'redirect_url' => "https://techtalkplanet.com/devftp/admin/events/thank-you.php?eventTitle=$event_title",
    "webhook" => "https://techtalkplanet.com/devftp/admin/events/webhook.php?eventId=$event_id&seats=$seats&bookingId=$bookingID",
    'send_email' => true,
    'send_sms' => true,
    'email' => $user['email'],
    'allow_repeated_payments' => false
);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
$response = curl_exec($ch);
curl_close($ch); 

$json_decode = json_decode($response, true);
$long_url = $json_decode['payment_request']['longurl'];
header("Location: $long_url"); } ?>


