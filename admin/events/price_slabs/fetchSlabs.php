<?php include('../../../config.php'); ?>
<?php
$table_name = $_POST['tname'];
$slabrecordsql = "SELECT * FROM $table_name";
$result = getMultipleRecords($slabrecordsql);
$data = "No Slabs Found";
$formID = 1;

if(!empty($result)) {
$data= null;
foreach($result as $key => $slab) {

    $data.="<form id='form$formID'><table class='table table-bordered'><thead><tr><th>Lower Limit</th><th>Upper Limit</th><th>Discount %</th><th>Action</th></tr></thead><tr><input type='hidden' value='$table_name' name='table' /><input type='hidden' value='".$slab['ID']."' name='slabID' />";
    $data.="<td><input type='number' name='slablowerlimit' value='".$slab['lower_limit']."' readonly form='form$formID'  min='0' />";
    $data.="</td>";
    $data.="<td><input onchange=saveUpperlimit($formID) type='number' name='slabupperlimit' value='".$slab['upper_limit']."' form='form$formID' min='0' />";
    $data.="</td>";
    $data.="<td><input onchange=saveDiscount('form$formID') name='slabdiscount' type='number' style='width:100%' min='0' value='".$slab['discount']."' form='form$formID'>";
    $data.="</input></td>";
    $slabId = $slab['ID'];
    $deleteButton =  ($key == count($result) - 1 ) ? "<span onclick='delSlab($slabId, \"$table_name\" )' class='btn btn-sm btn-danger'> <span class='glyphicon glyphicon-trash'></span></span>" : "";
    $data.= "<td>$deleteButton</td>";
    $data.="</tr></table></form>";

    $formID++;
}

$data.= "</tbody></table>";


}

echo $data;
?>