<script>var showSlab = false;
var addslab = false;
function displaySlabs(table_name) {
  if (!showSlab) {
    var tname = table_name;
    $.ajax({
      url: "fetchSlabs.php",
      method: "POST",
      data: { tname: tname },
      success: function(data) {
        $("#slabs").html(data);
      }
    });
  } else {
    $("#slabs").html("");
  }
  showSlab = !showSlab;
}

function addSlab(slabTable) {
    var newSlab = "<form class='form' method='post' >";
    newSlab+="<input type='hidden' name='table' value='"+slabTable+"' />";
    newSlab+= "<table class='table'>"; 
    newSlab+= "<thead><tr><th>Lower Limit</th><th>Upper Limit</th><th>Discount %</th><th></th></tr></thead>";
    newSlab+="<tbody>";
    newSlab+="<tr>";
    newSlab+="<td><div class='form-group <?php echo isset($errors['slablowerlimit']) ? 'has-error' : '' ?>'>";
    newSlab+="<input type='text' style='width:100%' name='slablowerlimit' <?php if($isEditing) echo "value='".$lowerlimit."'"; ?> />";
    newSlab+="<?php if (isset($errors['slablowerlimit'])): ?><span class='help-block'><?php echo $errors['slablowerlimit'] ?></span><?php endif; ?></div></td>";
    newSlab+="<td><div class='form-group <?php echo isset($errors['slabupperlimit']) ? 'has-error' : '' ?>'>";
    newSlab+="<input type='text' style='width:100%' name='slabupperlimit' <?php if($isEditing) echo "value='".$upperlimit."'"; ?> />";
    newSlab+="<?php if (isset($errors['slabupperlimit'])): ?><span class='help-block'><?php echo $errors['slabupperlimit'] ?></span><?php endif; ?></div></td>";
    newSlab+="<td><div class='form-group <?php echo isset($errors['slabdiscount']) ? 'has-error' : '' ?>'>";
    newSlab+="<input type='text' style='width:100%' name='slabdiscount' <?php if($isEditing) echo "value='".$discount."'"; ?> />";
    newSlab+="<?php if (isset($errors['slabdiscount'])): ?><span class='help-block'><?php echo $errors['slabdiscount'] ?></span><?php endif; ?></div></td>";
    newSlab+="<td>";
    newSlab+="<button type='submit' name='saveSlab' class='btn btn-success btn-block btn-lg'>Add</button>";
    newSlab+="</td>";
    newSlab+="</tr>";
    newSlab+= "</tbody></table>";
    newSlab+= "</form>"
  
  if(!addslab) {
  $('#newslab').html(newSlab);
  }
  else {
    $('#newslab').html("");
  }
  addslab = !addslab;
  }

function deleteSlabset(table) {
  if (
    confirm(
      "The Following Slabset will be deleted. Are you Sure You want to Proceed?"
    )
  ) {
    $.ajax({
      url: "slabList.php",
      method: "POST",
      data: { delTable: table }
    });
    window.location = window.location;
  }
}

function saveDiscount(formID) {
  $.ajax({
    url: "slabList.php",
    method: "POST",
    data: $("#" + formID).serialize() + "&updateSlab"
  });
}

function saveUpperlimit(formID) {
  fid = formID + 1;
  var nextUpperLimit = $("#form" + fid + " input[name='slabupperlimit']").val();
  var lowerLimit = $("#form" + formID + " input[name='slablowerlimit']").val();
  if (!(parseInt(event.target.value) < lowerLimit)) {
    if (nextUpperLimit != undefined) {
      if (parseInt(event.target.value) < parseInt(nextUpperLimit)) {
        $("#form" + formID + " input[name='slabupperlimit'] + span").remove();
        $("#form" + fid + " input[name='slablowerlimit']").attr(
          "value",
          parseInt(event.target.value) + 1
        );
        saveDiscount("form" + formID);
        saveDiscount("form" + fid);
      } else {
        $(
          "<span class='help-block' style='color:red'>This Should be Less Than Next Upper Limit</span>"
        ).appendTo(event.target.parentNode);
      }
    } else {
      saveDiscount("form" + formID);
    }
  }
}

function delSlab(id, table) {
  showSlab = false;
  $.ajax({
    url: "slabList.php",
    method: "POST",
    data: { delSlabId: id, tableName: table },
    success: function() {
      displaySlabs(table);
    }
  });
}
</script>