<?php include('../../../config.php'); ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php include('storeSlab.php'); 
$slabsets = isSuperAdmin() ? getPriceSlabsets() : getPriceSlabsetbyUser();?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Admin Area - Price Slabs </title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../../assets/css/style.css">
</head>
  <body style="margin: 5% 5% 5% 5%">
        
  <div class="col-md-8 col-md-offset-2">
  <?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
    <h1 class="text-center">Price Slab Sets</h1>
    <hr>
    <br />
    <div id="slabsets">
    <?php if (count($slabsets) > 0) : $i = 0; ?>
      <table class="table table-bordered">
        <thead>
          <tr>
          <th>Table</th>
          <?php if(isSuperAdmin()) :?>
          <th>Author</th>
          <?php endif; ?>
            <th colspan="2" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($slabsets as $slabset): ?>
            <tr>
            <td><?php echo ucwords(str_replace("price slab ", "", str_replace("_", " ", $slabset['table_name']))) ?>
         </td>
         <?php if(isSuperAdmin()) :?>
          <td><?php echo $slabset['username'] ?></td>
          <?php endif; ?>
<td class="text-center"><span onclick="displaySlabs('<?php echo $slabset['table_name'] ?>')" class="btn btn-sm btn-success">
                  <span class="glyphicon glyphicon-pencil"></span></span>
                  <span onclick="addSlab('<?php echo $slabset['table_name'] ?>')" class="btn btn-sm btn-success">
                  <span class="glyphicon glyphicon-plus"></span></span>
                  <span onclick="deleteSlabset('<?php echo $slabset['table_name'] ?>')" class="btn btn-sm btn-danger">
                  <span class="glyphicon glyphicon-trash"></span></span></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h2 class="text-center">No Slabsets exist</h2>
    <?php endif; ?>
  </div>     
  <div id="slabs">
  </div>
  <div id="newslab">
  </div>
  <br />
    <h3 class="text-center">OR</h3>
    <form class="form" id="newSlabset" method="post">
    <div class="form-group" style="display:flex;flex-direction:column" >
            <input type="text" style="flex:1" required placeholder="Slab Set Name"  name="new_slab_set_name" />
    </div>
    <div class="form-group">
            <button type="submit" class="btn btn-success btn-block btn-lg" name="create_slab_set">Create New Slabset</button>
            </div>
            </form>
  </div>
  <?php include(INCLUDE_PATH. '/layouts/footer.php') ; ?>
  <?php include("./ajaxcalls.php") ?>
  </body>
  
  </html>