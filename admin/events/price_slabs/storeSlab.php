<?php 
$user_id = $_SESSION['user']['id'];
$isEditing = false;
$largest = 1; 



if(isset($_POST['create_slab_set'])) { 
    $table_name = "price_slab_".str_replace(" ", "_", strtolower($_POST['new_slab_set_name']));
    $sql = "CREATE TABLE $table_name ( ID int AUTO_INCREMENT PRIMARY KEY, lower_limit int, upper_limit int, discount int )";
    if(@modifyRecord($sql , '' , [] )) {
        $sql = "INSERT INTO slab_sets SET user_id =? , table_name =?";
        @modifyRecord($sql , 'is' , [$user_id, $table_name] );
    }
}


if(isset($_POST['saveSlab'])) {
$lowerlimit = $_POST['slablowerlimit'];
$upperlimit = $_POST['slabupperlimit'];
$discount = $_POST['slabdiscount'];
$table_name = $_POST['table'];
$getallslabsql ="SELECT * FROM $table_name";
$getallslab = getMultipleRecords($getallslabsql);
$errors= validateSlab($_POST, ['saveSlab'], $getallslab);
if(!empty($table_name) && count($errors) === 0) {
    
$insertSlab = "INSERT INTO $table_name SET lower_limit= ?, upper_limit = ?, discount = ?";
if(modifyRecord($insertSlab, 'iii', [$lowerlimit, $upperlimit, $discount ])) {
    $_SESSION['success_msg'] = "Slab Added Successfully";
}
else {
    $_SESSION['error_msg'] = "Could not Add Slab ";
}
}

else {
    $isEditing=  true;
}
}

if(isset($_POST['updateSlab'])) {
    $lowerlimit = $_POST['slablowerlimit'];
    $discount = $_POST['slabdiscount'];
    $upperlimit = $_POST['slabupperlimit'];
    $id = $_POST['slabID'];
    $table_name = $_POST['table'];
    if(!empty($table_name)) {
    $updateSlab = "UPDATE $table_name SET discount = ?, upper_limit = ?, lower_limit= ? WHERE ID = ?";
    if(modifyRecord($updateSlab, 'iiii', [$discount,$upperlimit,$lowerlimit,$id])) {
        $_SESSION['success_msg'] = "Slab Added Successfully";
    }
    else {
        $_SESSION['error_msg'] = "Could not Add Slab ";
    }
    }
    }

if(isset($_POST['delTable'])) {
    $table = $_POST['delTable'];
   $sql = "DROP TABLE $table";
   if(@modifyRecord($sql, '', [])) {
       $sql = "DELETE FROM slab_sets WHERE table_name =?";
       modifyRecord($sql, 's', [$table]);
   }
}

if(isset($_POST['delSlabId']) && isset($_POST['tableName'])) {
    $table = $_POST['tableName'];
    $delslab = "DELETE FROM $table WHERE ID = ?";
    @modifyRecord($delslab, 'i',[$_POST['delSlabId']]); 
}


?>
