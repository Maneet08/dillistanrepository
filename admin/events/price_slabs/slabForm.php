<?php include('../../../config.php'); ?>
<?php include('storeSlab.php'); ?>
<!DOCTYPE html>
<html>
  <head>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <!-- Custome styles -->
    <link rel="stylesheet" href="../../../assets/css/style.css">
  </head>
  <body style="margin: 5% 5% 5% 5%">
    <?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="text-center" >Add New Price Slab</h1>
        
            <hr><br>
            <form class="form" method="post" >
            <div class="form-group">
            <label class="control-label"> Enter Lower Limit :</label>
            <input type="number" name="slablowerlimit" class="form-control" >
            </div>
            <div class="form-group">
            <label class="control-label"> Enter Upper Limit :</label>
            <input type="number" name="slabupperlimit" class="form-control" >
            </div>
            <div class="form-group">
            <label class="control-label"> Enter Discount : (In Percentage)</label>
            <input type="number" name="slabdiscount" class="form-control" >
            </div>

            <div class="form-group">
            <button type="submit" name="saveSlab" class="btn btn-success btn-block btn-lg">Submit</button>
            </div>
            </form>

    </div>
    </div>
    <?php include(INCLUDE_PATH. '/layouts/footer.php') ; ?>
 </body>
 </html>
