<?php include('../../config.php'); ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php include('eventLogic.php'); ?>
<?php 
$regions = getRegions();
$event_cats = geteventsCategory(); 
$priceslabs = getPriceSlabsetbyUser(); ?>
<!DOCTYPE html>
<html>
<head>
  
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custom styles -->
  <link rel="stylesheet" href="../../static/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
<div class="col-md-8 col-md-offset-2">
<?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
<form class="form" name="eform" method="post" enctype="multipart/form-data">
<?php //if ($isEditing === true): ?>
       <!-- <h1 class="text-center">Update Place</h1> -->
        <?php //else: ?>
        <h1 class="text-center">Create A New Event</h1>
        <?php //endif; ?>
        <hr><br />

      
        <div class="form-group <?php //echo isset($errors['ename']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Name : </label> 
                    <input type="text" name="ename" value="<?php //echo $name ?>"  class="form-control" required>
                    <?php //if (isset($errors['ename'])): ?>
                <span class="help-block"><?php// echo $errors['ename'] ?></span>
              <?php // endif; ?>
        </div>
        <div class="form-group <?php //echo isset($errors['edescription']) ? 'has-error': '' ?>">
                    <label  class="control-label" > Description : </label> 
                    <textarea  name="edescription"  class="form-control" required><?php //echo $description;?></textarea>
                    <?php //if (isset($errors['edescription'])): ?>
             <span class="help-block"><?php //echo $errors['edescription'] ?></span>
          <?php //endif; ?>    
        </div>   

        <div class="form-group <?php //echo isset($errors['edescription']) ? 'has-error': '' ?>">
                    <label  class="control-label" > Category : </label> 
                   <select class="form-control" name="ecategory" required>
                   <option>Select Category</option>
                   <?php
                   foreach($event_cats as $event_cat) : ?>
                  <option value="<?php echo $event_cat['ID'] ?>"><?php echo $event_cat['Name'] ?></option>
                   
                   <?php endforeach ?>
                   </select>
                    <?php //if (isset($errors['edescription'])): ?>
             <span class="help-block"><?php //echo $errors['edescription'] ?></span>
          <?php //endif; ?>    
          
        </div>

        <div class="form-group <?php echo isset($errors['region_id']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Choose Region : </label> 
                    <select id="r_id"  name="region_id"  class="form-control">
                            <option value="">Select Region</option>
                            <?php foreach ($regions as $region): ?>
                            <option value="<?php echo $region['id']; ?>" <?php //if($region['id'] == $region_id): echo "selected"; endif; ?>><?php echo $region['region_name']; ?></option>
                            <?php endforeach; ?>
                    </select>
                    <?php if (isset($errors['region_id'])): ?>
                <span class="help-block"><?php echo $errors['region_id'] ?></span>
              <?php endif; ?>
          </div>


          <div class="form-group <?php echo isset($errors['area_id']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Choose Area : </label> 
                    <select id="a"  name="area_id"  class="form-control" disabled>
                            
                    </select>
                    <?php if (isset($errors['area_id'])): ?>
                <span class="help-block"><?php echo $errors['area_id'] ?></span>
              <?php endif; ?>
          </div>  

          <div class="form-group <?php echo isset($errors['eaddress']) ? 'has-error' : '' ?>">
                    <label  class="control-label" > Address : </label> 
                    <textarea name="eaddress"  class="form-control" ><?php //echo $address;?></textarea>
                    <?php if (isset($errors['eaddress'])): ?>
                <span class="help-block"><?php echo $errors['eaddress'] ?></span>
              <?php endif; ?>
        </div>     


        <div class="form-group <?php //echo isset($errors['edescription']) ? 'has-error': '' ?>">
                    <label  class="control-label" >Starting From : </label> 
                    <input type="datetime-local" name="edatefrom"  class="form-control" required><?php //echo $description;?></textarea>
                    <?php //if (isset($errors['edescription'])): ?>
             <span class="help-block"><?php //echo $errors['edescription'] ?></span>
          <?php //endif; ?>    
        </div>   

        <div class="form-group <?php //echo isset($errors['edescription']) ? 'has-error': '' ?>">
                    <label  class="control-label" > Till : </label> 
                    <input type="datetime-local" name="edateto"  class="form-control"  required><?php //echo $description;?></textarea>
                    <?php //if (isset($errors['edescription'])): ?>
             <span class="help-block"><?php //echo $errors['edescription'] ?></span>
          <?php //endif; ?>    
        </div>   

        <div class="form-group <?php //echo isset($errors['edescription']) ? 'has-error': '' ?>">
                    <label  class="control-label" > Featured Image : </label> 
                    <input type="file" name="efimage"  class="form-control" ><?php //echo $description;?></textarea>
                    <?php //if (isset($errors['edescription'])): ?>
             <span class="help-block"><?php //echo $errors['edescription'] ?></span>
          <?php //endif; ?>    
        </div>  

        <div class="form-group <?php //echo isset($errors['edescription']) ? 'has-error': '' ?>">
                    <label  class="control-label" > Total Seats : </label> 
                    <input type="number" name="eseats"  class="form-control" required><?php //echo $description;?></textarea>
                    <?php //if (isset($errors['edescription'])): ?>
             <span class="help-block"><?php //echo $errors['edescription'] ?></span>
          <?php //endif; ?>    
        </div>

        <div class="form-group <?php //echo isset($errors['edescription']) ? 'has-error': '' ?>">
                    <label  class="control-label" > Ticket Price (INR): </label> 
                    <input type="number" name="eprice"  class="form-control" required><?php //echo $description;?></textarea>
                    <?php //if (isset($errors['edescription'])): ?>
             <span class="help-block"><?php //echo $errors['edescription'] ?></span>
          <?php //endif; ?>    
        </div>  

        <div class="form-group <?php //echo isset($errors['edescription']) ? 'has-error': '' ?>">
                    <label  class="control-label" > Price Slab : </label> 
                  <select name="epriceslab" class="form-control" required>
                  <option>Select Slab</option>
                  <?php foreach ($priceslabs as $priceslab) :?>
                  <option value="<?php echo $priceslab['table_name'] ?>"><?php echo ucwords(str_replace("price slab ", "" ,str_replace("_"," ",$priceslab['table_name']))) ?></option>
                  <?php endforeach; ?>
                  </select>
                    <?php //if (isset($errors['edescription'])): ?>
             <span class="help-block"><?php //echo $errors['edescription'] ?></span>
          <?php //endif; ?>    
        </div>  

        <div class="form-group">
          <?php // if ($isEditing): ?>
         <!--  <button  type="submit" name="update_event" class="btn btn-primary btn-block">Update Event</button> -->
          <?php //else: ?>
            <button  type="submit" name="save_event" class="btn btn-success btn-block">Create Event</button>
          <?php //endif; ?>
        </div>

</form>
</div>
<?php include(INCLUDE_PATH. '/layouts/footer.php') ; ?>
<script>
   $(document).ready(function () {
      $("#r_id").change(function () {
          
          var r_id = $(this).val();
          $.ajax({
              url: "<?php echo BASE_URL; ?>admin/Area/areabyRegion.php",
              method: "POST",
              data:{r_id:r_id},
              success: function(data) {
                  $("#a").html(data);
                  document.eform.area_id.disabled=false;
              }
          });
      });
   })
      </script>
</body>
</html>
