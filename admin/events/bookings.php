<?php include('../../config.php') ?>
<?php include(INCLUDE_PATH . '/logic/common_functions.php') ?>
<?php $bookings = isSuperAdmin() ? getBookingsforSuperAdmin() : getBookingsforAdmin($_SESSION['user']['id']) ;   ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Admin Area - Users </title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../../assets/css/style.css">
</head>
<body style="margin: 5% 5% 5% 5%">
<div class="col-md-12">
  <?php include(INCLUDE_PATH. '/layouts/messages.php') ; ?>
<h1 class="text-center">Bookings</h1>
<hr/>
<br>
<?php if($bookings) : ?>
<table class="table table-bordered">
<thead>
<th>Title</th>
<th>Category</th>
<?php if(isSuperAdmin()) : ?>
<th>Author</th>
<?php endif; ?>
<th>Customer Name</th>
<th>Customer Email</th>
<th colspan="1">Date</th>
<th>Seats</th>
<th>Price</th>
<th>Payment ID </th>

</thead>
<tbody>
<?php foreach($bookings as $booking) :?>
<tr>
<td><?php echo $booking['title']?></td>
<td><?php echo $booking['Name']?></td>
<?php if(isSuperAdmin()) : ?>
<td><?php echo $booking['author']?></td>
<?php endif; ?>
<td><?php echo $booking['username']?></td>
<td><?php echo $booking['email']?></td>
<td colspan="1"><?php echo $booking['booking_date']?></td>
<td><?php echo $booking['tickets_booked']?></td>
<td>₹ <?php echo $booking['total_amount']?></td>
<td><?php echo $booking['payment_id']?></td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php else : ?>
<h2 class="text-center">No Booking Found</h2>
<?php endif; ?>
</div>
<?php include(INCLUDE_PATH. '/layouts/footer.php') ; ?>
</body>
</html>