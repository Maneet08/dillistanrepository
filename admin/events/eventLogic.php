<?php
$title = '';
$description = '';
$fimg = '';
$category = null;
$startdate  = null;
$enddate  = null;
$totalseats = null;
$priceslab = "";



if(isset($_POST['save_event'])) {
    $title = $_POST['ename'];
    $description = $_POST['edescription'];
    $fimg  = uploadPicture("efimage","events");
    $category = $_POST['ecategory'];
    $region = $_POST['region_id'];
    $area = $_POST['area_id'];
    $address = $_POST['eaddress'];
    $startdate = $_POST["edatefrom"];
    $enddate= $_POST['edateto'];
    $totalseats = $_POST['eseats'];
    $price_per_seat = $_POST['eprice'];
    $priceslab = $_POST['epriceslab'];
    $insertSQL = "INSERT INTO events(author_id,title, description, category, region_id, area_id,address,start_date, end_date, image, seats, price_per_seat ,price_slab) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $insertEvent = modifyRecord($insertSQL, 'issiiissssiis', [$_SESSION['user']['id'],$title,$description,$category,$region,$area,$address,$startdate,$enddate,$fimg,$totalseats,$price_per_seat,$priceslab]);

if($insertEvent) {
    $_SESSION['success_msg'] = "New Event Created Successfully";
    header("Location: eventList.php");
}

else {
    $_SESSION['error_msg'] = "Something Went Wrong!!.. Please try again";
}

}

if(isset($_GET['event'])) {
    $sql = "SELECT * FROM events WHERE ID= ?";
    $event = getSingleRecord($sql, 'i', [$_GET['event']]);
}

?>