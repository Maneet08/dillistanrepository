<?php include('../config.php'); ?>
<?php include(ROOT_PATH . '/admin/middleware.php');?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <title>Admin</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
  <!-- Custome styles -->
  <link rel="stylesheet" href="../assets/css/style.css">
</head>
<body>
 <?php include(INCLUDE_PATH . "/layouts/admin_navbar.php") ?>
<div class="row" style="margin-right:unset">
 <div class="col-md-2" style="padding:0">
      
      <br />
      <ul class="list-group">
        <?php if(canManageUser()) : ?>
      
        <li class="dropdown-btn">
        <a  class="list-group-item ">User Operations</a></li>
        <div class="dropdown-container">
        <ul >
    <li><a id="tabVU" href="<?php echo BASE_URL . 'admin/users/userList.php' ?>" style="color:black">View Users</a></li>    
    <li><a id="tabCU" href="<?php echo BASE_URL . 'admin/users/userForm.php' ?>" style="color:black">Create New User</a></li>
    <li><a id="tabVR" href="<?php echo BASE_URL . 'admin/users/roles/roleList.php' ?>" style="color:black">User Roles</a></li>
    <li><a id="tabCR" href="<?php echo BASE_URL . 'admin/users/roles/roleForm.php' ?>" style="color:black">Create User Role</a></li>
    
    </ul>
  </div>
        <?php endif; ?>

         
        <?php  if(canManageAreas()) : ?>
        <li class="dropdown-btn">
        <a  class="list-group-item ">Areas</a></li>
        <div class="dropdown-container">
        <ul >
        <li><a id="tabVa" href="<?php echo BASE_URL . 'admin/Area/areaList.php' ?>" style="color:black">View Areas</a></li>  
        <li><a id="tabAa" href="<?php echo BASE_URL . 'admin/Area/areaForm.php' ?>" style="color:black">Add a New Area</a></li>
        </ul>
        </div>
        <?php  endif; ?>
        
        <?php if(canManageListing()) : ?>
        <li class="dropdown-btn">
        <a  class="list-group-item">Explore</a></li>
        <div class="dropdown-container">
        <ul >
        <?php if(canManageCategories()) :?>
        <li><a id="tabVc" href="<?php echo BASE_URL . 'admin/explore/Category/categoryList.php' ?>" style="color:black">View Listing Category</a></li> 
        <li><a id="tabAc" href="<?php echo BASE_URL . 'admin/explore/Category/categoryForm.php' ?>" style="color:black">Create New Category</a></li>
        <?php endif; ?>
        <li><a id="tabVl" href="<?php echo BASE_URL . 'admin/explore/placeList.php' ?>" style="color:black">View Listings</a></li>
        <li><a id="tabAl" href="<?php echo BASE_URL . 'admin/explore/placeForm.php' ?>" style="color:black">Add a New Listing</a></li>
    
    
    </ul>
  </div>
        <?php endif; ?>

       
        <li class="dropdown-btn">
        <a  class="list-group-item">Events</a></li>
        <div class="dropdown-container">
        <ul >
        <?php if(canManageEvents()) : ?>
        <?php if(canManageCategories()) :?>
        <li><a id="tabVEC" href="<?php echo BASE_URL . 'admin/events/Category/categoryList.php' ?>" style="color:black">View Event Category</a></li> 
        <li><a id="tabAEC" href="<?php echo BASE_URL . 'admin/events/Category/categoryForm.php' ?>" style="color:black">Create New Category</a></li>
        <?php endif; ?>
        <li><a id="tabVs" href="<?php echo BASE_URL . 'admin/events/price_slabs/slabList.php' ?>" style="color:black">Price Slabs</a></li>
        <li><a id="tabVe" href="<?php echo BASE_URL . 'admin/events/eventList.php' ?>" style="color:black">View Events</a></li>
        <li><a id="tabAe" href="<?php echo BASE_URL . 'admin/events/eventForm.php' ?>" style="color:black">Create a New Event</a></li>
        <li><a id="tabVb" href="<?php echo BASE_URL . 'admin/events/bookings.php' ?>" style="color:black">Bookings</a></li>
        <li><a id="tabRbc" href="<?php echo BASE_URL . 'admin/events/cancelledBookings.php' ?>" style="color:black">Cancelled Booking</a></li>
        <?php else : ?>
        <li><a id="tabMb" href="<?php echo BASE_URL . 'admin/events/bookingforUser.php' ?>" style="color:black">My Bookings</a></li>
        <?php endif; ?>
          
    
    </ul>
  </div>
        
        

      
      </ul>
  </div>

   <div class="col-md-10" style="padding:0">
  <div id="preloader">
    <img src="http://blog.chapagain.com.np/examples/jquery-ajax-tabs/loading.gif" align="absmiddle"> Loading...
</div> 
  <iframe id="content" style="width:100%;height:100vh"></iframe> 

</div>
</div>

 
  <?php include(INCLUDE_PATH . '/layouts/footer.php') ?>
<script>


$(document).ready(function(){
    $("#preloader").hide();

    var dl = $(".dropdown-btn");
    dl.each(function(){
      $(this).find('a').click(function(){
      var dropdownContent = $(this).parent().next();
      if(dropdownContent.css("display") == "none") {
      dropdownContent.css("display", "block");
      }
      else {
        dropdownContent.css("display", "none");
      }
   })
      }
    );


$("[id^=tab]").click(function(){
       // get tab id and tab url
        tabId = $(this).attr("id");
        tabUrl = $("#"+tabId).attr("href");
        jQuery("[id^=tab]").removeClass("current");
        // load tab content
        loadTabContent(tabUrl);
        return false;
   });
    });

function loadTabContent(tabUrl){
    $("#content").attr("src", tabUrl);  
}
</script>
<style> .list-group > li > a {padding-left: 40px;padding-right:10px} </style>
</body>
</html>