<?php 
 $areas = array();
 $allregions = array();

// USER

function isSuperAdmin() {
  
    if($_SESSION['user']['role'] == "Super Admin") {
        return true;
    }
    else {
        return false;
    }
}

function isAdmin() {
  if($_SESSION['user']['role'] == "Admin") {
      return true;
  }
  else {
      return false;
  }
}


function ifAdmin($user_id) {
  global $conn;
  $sql = "SELECT id FROM users WHERE id =? AND role_id = 1 OR 5";
  $user = getSingleRecord($sql, 'i', [$user_id]); // get single user from database
  if(!empty($user)) {
      return true;
  }
  else {
      return false;
  }
}

  function loginById($user_id) {
      global $conn;
      $sql = "SELECT u.id, u.role_id, u.username, u.profile_picture, r.name as role FROM users u LEFT JOIN roles r ON u.role_id=r.id WHERE u.id=? LIMIT 1";
      $user = getSingleRecord($sql, 'i', [$user_id]);

      if(!empty($user)) {
          // put logged in user into session array
          $_SESSION['user'] = $user;
          $_SESSION['success_msg'] = "You are now logged in";
         
          // if user is admin, redirect to dashboard, otherwise to homepage
         
             $permissionsSql = "SELECT p.name as permission_name FROM permissions as p JOIN permission_role as pr ON p.id=pr.permission_id WHERE pr.role_id=?";
             $userPermissions = getMultipleRecords($permissionsSql, 'i' , [$user['role_id']]);
             $_SESSION['userPermissions'] = $userPermissions;
             header('location: ' . BASE_URL . 'admin');
         
             
      }
  }

  function validateRole($role, $ignoreFields) {
    global $conn;
    $errors = [];

    foreach ($role as $key => $value) {
      if (in_array($key, $ignoreFields)) {
          continue;
      }
      if (empty($role[$key])) {
        $errors[$key] = "This field is required";
      }
    }
    return $errors;
}


   // Accept a user object, validates user and return an array with the error messages

      function validateUser($user, $ignoreFields) {
          global $conn;
          $errors = [];
           
          // password confirmation
           if(isset($user['passwordConf']) && ($user['password'] !== $user['passwordConf']  )) {
               $errors['passwordConf'] = "The two passwords do not match ";
           }

           // if passwordOld was sent, then verify old password
           if(isset($user['passswordOld']) && isset($user['user_id'])) {
               $sql = "SELECT * FROM users WHERE id=? LIMIT 1";
               $oldUser = getSingleRecord($sql , "i", [$user['user_id']]);
               $prevPasswordHash = $oldUser['password'];

               if(!password_verify($user['passwordOld'], $prevPasswordHash)) {
                   $errors['passwordOld'] = "The old password does not match";
               }

           }

           // the email should be unique for each user for cases where we are saving admin user or signing up new user
           if(in_array('save_user', $ignoreFields) || in_array('signup_btn' , $ignoreFields)) {

            $sql = "SELECT * FROM users WHERE email=? OR username=? LIMIT 1";
            $oldUser = getSingleRecord($sql, 'ss', [$user['email'], $user['username']]);

            if(!empty($oldUser['email']) && $oldUser['email'] === $user['email'] ) {
                 //if user exists
                 
                 $errors['email'] = "Email already exists";
            }

            if(!empty($oldUser['username']) && $oldUser['username'] === $user['username']) {
                // if user exists
                $errors['username'] = "Username already exists";
            }
           }

           // required validation 
            
        
  	  foreach ($user as $key => $value) {
        if (in_array($key, $ignoreFields)) {
          continue;
        }
  			if (empty($user[$key])) {
  				$errors[$key] = "This field is required";
  			}
  	  }
  		return $errors;
  }

  // USER END

  // UPLOAD FILES


 function moveUploadedFile($filename,$tmpfilename,$root) {
  $picture = date("Y.m.d") . $filename;
  // define Where image will be stored
      $target = ROOT_PATH . "/assets/images/" . $root ."/" . $picture;
  // upload image to folder
      if( move_uploaded_file($tmpfilename, $target)) {
      return $picture;
              exit();
      }
      else {
        $_SESSION['error_msg'] = "There was a error uploading your image";
      }
 } 

    // upload's user profile profile picture and returns the name of the file 
    function uploadPicture($type,$root) {
        // if file was sent from signup form ...
        if(!empty($_FILES[$type]['name']) ) { 
        // Get image name
         $pic = moveUploadedFile($_FILES[$type]['name'],$_FILES[$type]['tmp_name'],$root);
         return $pic;  
        }
      }

      function uploadMultiplePicture($type,$root) {
        
            if(!empty($_FILES[$type]['name'])){

              foreach(array_combine($_FILES[$type]['name'], $_FILES[$type]['tmp_name']) as $Iname => $ITname){
                $files[] = moveUploadedFile($Iname,$ITname,$root);
              }
              return $files;      
            }
      }     

// UPLOAD FILES END


// AREAS & REGION

 function validateArea($area, $ignoreFields) {

    $errors = [];
    if(in_array('save_area', $ignoreFields)) {

    $sql = "SELECT * FROM area WHERE area_name=? LIMIT 1";
    $oldArea = getSingleRecord($sql, 's', [$area['area_name']]);
    if(!empty($oldArea)) {
        $errors['area_name'] = "Area Already Exists";
    }
     }

    foreach ($area as $key => $value) {

        if (in_array($key, $ignoreFields)) {
            continue;
          }
  			if (empty($area[$key])) {
  				$errors[$key] = "This field is required";
  			}
  	  }
  		return $errors;

 }

 function getAreas() {
  $sql = "SELECT a.id, a.area_name, a.region_id, r.region_name FROM area a JOIN region r ON a.region_id=r.id";
  $areas = getMultipleRecords($sql);
  return $areas;
} 

function getRegions() {
  $sql = "SELECT * FROM region";
  $allregions = getMultipleRecords($sql);
  return $allregions;
}

// AREAS & REGION END 


// EXplore OR Listing Module


function validateCat($cat, $ignoreFields) {
  global $conn;
  $errors = [];

  if(isset($cat['cat_id'])) {
      $sql = "SELECT * FROM category WHERE name=? AND id !=? LIMIT 1";    
  $oldUs = getSingleRecord($sql, 'si', [$cat['name'], $cat['cat_id']]);
  }

  else {
  $sql = "SELECT * FROM category WHERE name=? LIMIT 1";    
  $oldUs = getSingleRecord($sql, 's', [$cat['name']]);
  }


  if(!empty($oldUs)) {
      $errors['name'] = "Category Already Exists";
  }

  foreach ($cat as $key => $value) {
    if (in_array($key, $ignoreFields)) {
        continue;
    }
    if (empty($cat[$key])) {
      $errors[$key] = "This field is required";
    }
  }
  return $errors;
}

function getAllCategory(){
  $sql = "SELECT id, name FROM category";
  $category = getMultipleRecords($sql);
  return $category;
}

function getAllCatType(){
  $sql = "SELECT c.* FROM cat_type c";
  $cat_type = getMultipleRecords($sql);
  return $cat_type;
} 

function getCategoryAllType($cat_id){
  $sql = "SELECT cat_type.* FROM cat_type
          JOIN cat_type_map
            ON cat_type.id = cat_type_map.type_id
          WHERE cat_type_map.cat_id=?";
  $types = getMultipleRecords($sql, 'i', [$cat_id]);
  return $types;
}


 function validatePlace($places, $ignoreFields) {
  $errors = [];

  if(in_array('save_place', $ignoreFields)) {

    $sql = "SELECT * FROM listings WHERE contact=?";
    $oldPlace = getSingleRecord($sql, 'i', [$places['pnumber']]);

    if(!empty($oldPlace['contact']) && $oldPlace['contact'] == $places['pnumber'] ) {
         //if number exists
         
         $errors['pnumber'] = "Number already exists in database";
    }
  }

  foreach ($places as $key => $value) {
    if (in_array($key, $ignoreFields)) {
        continue;
      }
    if (empty($places[$key])) {
      $errors[$key] = "This field is required";
    }
  }
  return $errors;
}
  
  // EXplore OR Listing MOdule End

  //Events Module
   function validateSlab($slab, $ignoreFields, $slabsets) {
    $errors = [];
    if(!($slab['slabupperlimit'] >= $slab['slablowerlimit'])) {
      $errors['slabupperlimit'] = "Upper Limit Should be Greater than Lower Limit";
    }
    if(count($slabsets) > 0) {
    if($slab['slablowerlimit'] != ($slabsets[count($slabsets) - 1]['upper_limit'] + 1) )
    {
      $errors['slablowerlimit'] = "New Lower Slab Limit Must Be Plus 1 of Last Upper Limit";  
    }
    }
    foreach ($slab as $key => $value) {
      if (in_array($key, $ignoreFields)) {
          continue;
        }
      if (empty($slab[$key])) {
        $errors[$key] = "This field is required";
      }
    }
 return $errors;
  }

  function geteventsCategory() {
    $catQuery = "SELECT * FROM events_cat";
  return  @getMultipleRecords($catQuery) ? @getMultipleRecords($catQuery): null;
  }

  function getPriceSlabsetbyUser() {
    $table_sql = "SELECT table_name FROM slab_sets WHERE user_id = ?";
$slabsets = getMultipleRecords($table_sql, 'i', [$_SESSION['user']['id']]);
return $slabsets;
  }

  function getPriceSlabsets() {
    $table_sql = "SELECT table_name, username FROM slab_sets JOIN users ON slab_sets.user_id = users.id";
$slabsets = getMultipleRecords($table_sql);
return $slabsets;
  }

  function getEventsbyUserId($user_id) {
    $getEventsSQL = "SELECT e.* , c.name , a.area_name FROM events e JOIN category c ON e.category=c.id JOIN area a ON e.area_id = a.id WHERE author_id = ?";  
    $events = getMultipleRecords($getEventsSQL, "i", [$user_id]);
    return $events;
  }

 function getEvents() {
    $getEventsSQL = "SELECT e.* , c.name , a.area_name , u.username FROM events e JOIN category c ON e.category=c.id JOIN area a ON e.area_id = a.id JOIN users u ON e.author_id = u.id";
    $events = getMultipleRecords($getEventsSQL);
    return $events;
  }

  function getBookingsforSuperAdmin() {
    $getEventBookingsSQL = "SELECT eb.* , u.username, u.email, a.username as author, e.title, ec.Name FROM event_booking eb JOIN users u ON eb.user_id = u.id JOIN events e ON eb.event_id = e.ID JOIN users a ON e.author_id = a.id JOIN events_cat ec ON e.category = ec.ID WHERE eb.booking_status = ?";
    $bookings = getMultipleRecords($getEventBookingsSQL, 's', ['booked']);
    return $bookings;
  }

  function getBookingsforAdmin($admin_id) {
    $getEventBookingsSQL = "SELECT eb.* , u.username, u.email, e.title, ec.Name FROM event_booking eb JOIN users u ON eb.user_id = u.id JOIN events e ON eb.event_id = e.ID JOIN events_cat ec ON e.category = ec.ID WHERE eb.booking_status = ? AND e.author_id = ?";
    $bookings = getMultipleRecords($getEventBookingsSQL, 'si', ['booked',$admin_id]);
    return $bookings;
  }

  function getCancelledBookingsforSuperAdmin() {
    $getCancelledEventBookingsSQL = "SELECT eb.* , u.username, u.email, a.username as author, e.title, ec.Name FROM event_booking eb JOIN users u ON eb.user_id = u.id JOIN events e ON eb.event_id = e.ID JOIN users a ON e.author_id = a.id JOIN events_cat ec ON e.category = ec.ID WHERE eb.booking_status = ? OR eb.booking_status = ?";
    $cancelledbookings = getMultipleRecords($getCancelledEventBookingsSQL, 'ss', ['admin cancellation approval pending', 'cancelled']);
    return $cancelledbookings;
  }

  function getCancelledBookingsforAdmin($admin_id) {
    $getCancelledEventBookingsSQL = "SELECT eb.* , u.username, u.email, a.username as author, e.title, ec.Name FROM event_booking eb JOIN users u ON eb.user_id = u.id JOIN events e ON eb.event_id = e.ID JOIN users a ON e.author_id = a.id JOIN events_cat ec ON e.category = ec.ID WHERE eb.booking_status = ? OR eb.booking_status = ? AND e.author_id = ?";
    $cancelledbookings = getMultipleRecords($getCancelledEventBookingsSQL, 'ssi', ['admin cancellation approval pending', 'cancelled', $admin_id]);
    return $cancelledbookings;
  }

  
  function getBookingsforUser($admin_id) {
    $getEventBookingsSQL = "SELECT eb.* , e.title, ec.Name FROM event_booking eb JOIN events e ON eb.event_id = e.ID JOIN events_cat ec ON e.category = ec.ID WHERE eb.booking_status = ? OR eb.booking_status = ? AND eb.user_id = ?";
    $bookings = getMultipleRecords($getEventBookingsSQL, 'ssi', ['booked', 'admin cancellation approval pending' , $admin_id]);
    return $bookings;
  }