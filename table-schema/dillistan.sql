-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 27, 2019 at 10:42 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dillistan`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `area_name` varchar(255) NOT NULL,
  `region_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `area_name`, `region_id`) VALUES
(1, 'Green Park', 2),
(5, 'RK Puram Sector - 1', 2),
(6, 'RK Puram Sector - 2', 2);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Restaurants'),
(2, 'Gym'),
(3, 'Spa'),
(4, 'Salon'),
(5, 'NGO'),
(6, 'Pet Adoption'),
(7, 'Clubs & Bars'),
(8, 'Hospitals'),
(9, 'Doctors');

-- --------------------------------------------------------

--
-- Table structure for table `cat_type`
--

CREATE TABLE `cat_type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cat_type`
--

INSERT INTO `cat_type` (`id`, `type_name`) VALUES
(1, 'Social Clubs'),
(2, 'Bars'),
(3, 'Day'),
(4, 'Night'),
(5, '24x7'),
(6, 'Unisex'),
(7, 'Men'),
(8, 'Women'),
(9, 'Children'),
(10, 'Cancer'),
(11, 'Natural Disaster'),
(12, 'Others'),
(13, 'Dogs'),
(14, 'Govt'),
(15, 'Private');

-- --------------------------------------------------------

--
-- Table structure for table `cat_type_map`
--

CREATE TABLE `cat_type_map` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cat_type_map`
--

INSERT INTO `cat_type_map` (`id`, `cat_id`, `type_id`) VALUES
(35, 7, 1),
(36, 7, 2),
(37, 2, 3),
(38, 2, 4),
(39, 2, 5),
(40, 4, 6),
(41, 4, 7),
(42, 4, 8),
(43, 5, 9),
(44, 5, 10),
(45, 5, 11),
(46, 5, 12),
(47, 6, 12),
(48, 6, 13),
(49, 8, 14),
(50, 8, 15),
(51, 9, 14),
(52, 9, 15);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `ID` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(400) NOT NULL,
  `description` varchar(200) NOT NULL,
  `category` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `address` varchar(250) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `image` varchar(100) NOT NULL,
  `seats` int(11) NOT NULL,
  `price_per_seat` int(11) NOT NULL,
  `price_slab` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`ID`, `author_id`, `title`, `description`, `category`, `region_id`, `area_id`, `address`, `start_date`, `end_date`, `image`, `seats`, `price_per_seat`, `price_slab`) VALUES
(6, 39, 'Sunburn', 'lore ipsum dolomer', 4, 2, 1, 'lorem ipsum ', '2019-05-02 23:59:00', '2019-12-31 23:59:00', '2019.08.19IMG_20190814_195241.jpg', 0, 30, 'price_slab_sunburn');

-- --------------------------------------------------------

--
-- Table structure for table `events_cat`
--

CREATE TABLE `events_cat` (
  `ID` int(11) NOT NULL,
  `Name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events_cat`
--

INSERT INTO `events_cat` (`ID`, `Name`) VALUES
(2, 'Hikiing'),
(4, 'Concert');

-- --------------------------------------------------------

--
-- Table structure for table `event_booking`
--

CREATE TABLE `event_booking` (
  `ID` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `booking_date` date NOT NULL,
  `tickets_booked` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL,
  `booking_status` enum('booked','cancelled') NOT NULL,
  `refund_status` enum('processing','processed','declined') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listings`
--

CREATE TABLE `listings` (
  `listing_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `contact` int(11) NOT NULL,
  `feature_image` varchar(255) NOT NULL,
  `description` varchar(400) NOT NULL,
  `open_time` time NOT NULL,
  `close_time` time NOT NULL,
  `area_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listings`
--

INSERT INTO `listings` (`listing_id`, `region_id`, `category_id`, `name`, `address`, `contact`, `feature_image`, `description`, `open_time`, `close_time`, `area_id`) VALUES
(29, 2, 1, 'TL12', 'lidi', 1234, '', 'lidi', '15:01:00', '17:01:00', 1),
(30, 2, 1, 'TL', 'lidi', 12345, '', 'lidi', '15:03:00', '18:03:00', 1),
(31, 2, 1, 'LT', 'lidi', 987, '2019.06.115_104__DSC0382.jpg', 'lidi', '15:05:00', '14:05:00', 5);

-- --------------------------------------------------------

--
-- Table structure for table `listings_gallery`
--

CREATE TABLE `listings_gallery` (
  `S.No` int(11) NOT NULL,
  `listingId` int(11) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listing_addon`
--

CREATE TABLE `listing_addon` (
  `id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `Cuisine` varchar(250) DEFAULT NULL,
  `Delivery_Time` int(11) DEFAULT NULL,
  `Charges` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listing_addon`
--

INSERT INTO `listing_addon` (`id`, `type_id`, `Cuisine`, `Delivery_Time`, `Charges`) VALUES
(31, NULL, 'All', 70, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `description`) VALUES
(1, 'publish-post', 'User can publish Post'),
(2, 'delete-post', 'User can delete Post'),
(3, 'manage-user', 'Can add a new user'),
(4, 'manage-roles', ''),
(5, 'manage-listing', ''),
(6, 'manage-areas', '');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `role_id`, `permission_id`) VALUES
(22, 1, 3),
(23, 1, 4),
(24, 1, 5),
(25, 1, 6),
(26, 5, 3),
(27, 5, 4),
(28, 5, 5),
(29, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `price_slab_sunburn`
--

CREATE TABLE `price_slab_sunburn` (
  `ID` int(11) NOT NULL,
  `lower_limit` int(11) DEFAULT NULL,
  `upper_limit` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_slab_sunburn`
--

INSERT INTO `price_slab_sunburn` (`ID`, `lower_limit`, `upper_limit`, `discount`) VALUES
(1, 5, 8, 20);

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `region_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `region_name`) VALUES
(5, 'Central Delhi'),
(3, 'East Delhi'),
(1, 'North Delhi'),
(2, 'South Delhi'),
(4, 'West Delhi');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`) VALUES
(1, 'Admin', 'Has authority of users and roles and permissions.'),
(2, 'Member', '..'),
(4, 'User', 'Website Visitor'),
(5, 'Super Admin', 'The Super Admin has all the permission and is basically owner of the site.');

-- --------------------------------------------------------

--
-- Table structure for table `slab_sets`
--

CREATE TABLE `slab_sets` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `table_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slab_sets`
--

INSERT INTO `slab_sets` (`id`, `user_id`, `table_name`) VALUES
(12, 39, 'price_slab_sunburn');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `username`, `email`, `password`, `profile_picture`, `created_at`, `updated_at`) VALUES
(38, 4, 'Maneet', '123@xyz.com', '$2y$10$Mj8nNEl5DDf72ITJbka0luqe74fPeu.9AuWZdPa9aHqQJwlr3qoAq', '2019.05.06IMG-20190327-WA0001 (1).jpg', '2019-05-06 11:32:18', '0000-00-00 00:00:00'),
(39, 5, 'admin', 'abhimanyutokas@gmail.com', '$2y$10$1DSc5q4a4oKu6dpLBjVlfuHe2mesErd5OUSznmX5qKBP8HFbL2Gh.', '2019.05.17.2019.05.06IMG-20190327-WA0003.jpg', '2019-07-26 08:55:57', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `area_name` (`area_name`),
  ADD KEY `region_id` (`region_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cat_type`
--
ALTER TABLE `cat_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cat_type_map`
--
ALTER TABLE `cat_type_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_ibfk_3` (`cat_id`),
  ADD KEY `permission_role_ibfk_4` (`type_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `events_cat`
--
ALTER TABLE `events_cat`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `event_booking`
--
ALTER TABLE `event_booking`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `listings`
--
ALTER TABLE `listings`
  ADD PRIMARY KEY (`listing_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `region_ibfk` (`region_id`),
  ADD KEY `area_ibfk` (`area_id`);

--
-- Indexes for table `listings_gallery`
--
ALTER TABLE `listings_gallery`
  ADD PRIMARY KEY (`S.No`);

--
-- Indexes for table `listing_addon`
--
ALTER TABLE `listing_addon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_ibfk` (`type_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- Indexes for table `price_slab_sunburn`
--
ALTER TABLE `price_slab_sunburn`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `region_name` (`region_name`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slab_sets`
--
ALTER TABLE `slab_sets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `users_ibfk_1` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cat_type`
--
ALTER TABLE `cat_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `cat_type_map`
--
ALTER TABLE `cat_type_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `events_cat`
--
ALTER TABLE `events_cat`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `event_booking`
--
ALTER TABLE `event_booking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `listings`
--
ALTER TABLE `listings`
  MODIFY `listing_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `listings_gallery`
--
ALTER TABLE `listings_gallery`
  MODIFY `S.No` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `listing_addon`
--
ALTER TABLE `listing_addon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `price_slab_sunburn`
--
ALTER TABLE `price_slab_sunburn`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `slab_sets`
--
ALTER TABLE `slab_sets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `area`
--
ALTER TABLE `area`
  ADD CONSTRAINT `area_ibfk_1` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`);

--
-- Constraints for table `cat_type_map`
--
ALTER TABLE `cat_type_map`
  ADD CONSTRAINT `permission_role_ibfk_3` FOREIGN KEY (`cat_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_ibfk_4` FOREIGN KEY (`type_id`) REFERENCES `cat_type` (`id`);

--
-- Constraints for table `listings`
--
ALTER TABLE `listings`
  ADD CONSTRAINT `area_ibfk` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `listings_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `region_ibfk` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`);

--
-- Constraints for table `listing_addon`
--
ALTER TABLE `listing_addon`
  ADD CONSTRAINT `type_ibfk` FOREIGN KEY (`type_id`) REFERENCES `cat_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
